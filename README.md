# lerna-bsh

### bsh utility modules

## Instructions

#### yarn

Installs dependencies for all modules

#### yarn del

Removes node_modules/**, package-lock.json, yarn.lock, and *.tgz

#### yarn build

Reinstalls all dependencies, runs all tests, runs build script for each module
