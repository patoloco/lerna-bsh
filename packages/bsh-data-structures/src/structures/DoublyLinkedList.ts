import { ListNode } from "./ListNode";
import { SinglyLinkedList } from "./SinglyLinkedList";

export class DoublyLinkedList<T> extends SinglyLinkedList<T> {

  constructor() {
    super();
  }

  public pushEnd(data: T) {
    const tmp = new ListNode<T>(data);
    tmp.setNext(null);

    if (this.isEmpty()) {
      this.head = this.tail = tmp;
    } else {
      this.tail.setNext(tmp);
      tmp.setPrev(this.tail);
      this.tail = tmp;
    }
    this.length++;
  }

  public pushFront(data: T) {
    const tmp = new ListNode<T>(data);
    if (this.isEmpty()) {
      this.head = this.tail = tmp;
    } else {
      tmp.setNext(this.head);
      this.head.setPrev(tmp);
      this.head = tmp;
    }
    this.length++;
  }

  public searchAndDelete(val: any, key?: string): any {
    const item = this.search(val, key);
    if (!item || !item.node) {
      return null;
    }

    const current = item.node;
    const prev = item.prev ? item.prev : null;
    const next = item.node && item.node.getNext() ? item.node.getNext() : null;

    // in the middle of a list
    if (next && prev) {
      prev.setNext(next);
      next.setPrev(prev);
      current.setNext(null);
      current.setPrev(null);
      this.length--;
    } else if (next && !prev) {
      // first item among others in a list
      next.setPrev(null);
      current.setNext(null);
      this.head = next;
      this.length--;
    } else if (!next && prev) {
      // last item among others in a list
      current.setPrev(null);
      prev.setNext(null);
      this.tail = prev;
      this.length--;
    } else {
      // no next, no prev = only item
      this.head = new ListNode(null);
      this.tail = new ListNode(null);
      this.length--;
    }
  }

  public deleteFirst(): any {
    if (this.getLength() === 0) {
      return;
    } else if (this.getLength() === 1) {
      const tmp = new ListNode(null);
      this.head = this.tail = tmp;
      this.length--;
      return;
    }
    const next = this.head.getNext();
    this.head.setNext(null);
    if (next) {
      next.setPrev(null);
      this.head = next;
    }
    this.length--;
  }

  public getListReverse(): any[] {
    const data: T[] = [];
    let tmp: any;
    tmp = this.tail;
    data.push(tmp.getData());
    while (tmp.getPrev()) {
      tmp = tmp.getPrev();
      data.push(tmp.getData());
    }
    return data;
  }

}
