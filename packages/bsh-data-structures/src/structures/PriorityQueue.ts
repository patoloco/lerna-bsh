import { Queue } from "./Queue";

export const MAX_PRIORITY = 0;

export class PriorityQueue extends Queue {

  constructor() {
    super();
  }

  public load(item: any, priority: number): number {
    if (priority < 0) {
      return -1;
    }

    const pItem: PriorityQueueItem = { item, priority };

    if (this.isEmpty()) {
      this.q.push(pItem);
      return 0;
    }

    const len = this.q.length;
    let i = 0;
    for (; i < len; i++) {
      if (this.q[i].priority > priority) {
        this.q.splice(i, 0, pItem);
        return i;
      }
    }

    this.q.push(pItem);
    return i + 1;
  }

  public merge(qq: PriorityQueue): void {
    if (qq.isEmpty()) {
      return;
    }

    const first = qq.next();
    let index = this.load(first.item, first.priority);

    while (!qq.isEmpty()) {
      const next = qq.next();
      const pItem: PriorityQueueItem = { item: next.item, priority: next.priority };

      if (!this.q[index]) {
        this.q.push(pItem);
      } else {
        let merged = false;
        while (this.q[index]) {
          if (this.q[index].priority > pItem.priority) {
            this.q.splice(index, 0, pItem);
            merged = true;
            break;
          }
          index++;
        }
        if (!merged) {
          this.q.push(pItem);
        }
      }
    }

  }

  public next(): any {
    if (!this.isEmpty()) {
      return this.q.shift();
    }
  }
}

export interface PriorityQueueItem {
  priority: number;
  item: any;
}
