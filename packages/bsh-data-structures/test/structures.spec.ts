// tslint:disable:no-console //

import * as chai from "chai";
import * as chaiAsPromised from "chai-as-promised";
import "mocha";

chai.use(chaiAsPromised);
const expect = chai.expect;
chai.should();

import { CircularLinkedList, DoublyLinkedList, GraphNode, ListSearchResponse,
  PriorityQueue, Queue, SinglyLinkedList, Stack } from "../src/structures";

import { MAX_PRIORITY } from "../src/structures";

describe("Structures", () => {
  describe("CircularLinkedList", () => {
    it("can create a circular list and add a single item", async () => {
      const list = new CircularLinkedList();
      list.pushEnd(1);

      expect(list.getLength()).to.equal(1);
      expect(list.getHead()).to.equal(list.getTail());
      expect(list.getHead().getData()).to.equal(1);
      expect(list.getHead().getNext().getData()).to.equal(1);
    });

    it("can create a circular list and add items", async () => {
      const list = new CircularLinkedList();
      list.pushEnd(1);
      list.pushEnd(2);
      list.pushEnd(3);
      list.pushEnd(4);
      list.pushEnd(5);

      expect(list.getLength()).to.equal(5);
      expect(list.getTail().getNext().getData()).to.equal(1);
      expect(list.getHead().getNext().getNext().getNext().getNext().getNext().getData()).to.equal(1);
    });

    it("can insert at the front of the list", async () => {
      const list = new CircularLinkedList();
      list.pushEnd(1);
      list.pushEnd(2);
      list.pushEnd(3);
      list.pushFront(0);
      list.pushFront(-1);

      expect(list.getLength()).to.equal(5);
      expect(list.getHead().getData()).to.equal(-1);
      expect(list.getTail().getData()).to.equal(3);
      expect(list.getTail().getNext().getData()).to.equal(-1);
    });

    it("can find and insert after a Node with primitive data", async () => {
      const list = new CircularLinkedList();
      list.pushEnd(1);
      list.pushEnd(4);
      list.pushEnd(9);

      const insertOp = list.insertAfterPrimitive(5, 4);
      expect(insertOp).to.equal(true);
      expect(list.getLength()).to.equal(4);

      const dataList = list.getList();
      expect(dataList[0]).to.equal(1);
      expect(dataList[1]).to.equal(4);
      expect(dataList[2]).to.equal(5);
      expect(dataList[3]).to.equal(9);
    });

    it("can correctly insert after an item in a single-item list", async () => {
      const list = new CircularLinkedList();
      list.pushEnd("test");

      const insertOp = list.insertAfterPrimitive("testTwo", "test");
      expect(insertOp).to.equal(true);
      expect(list.getLength()).to.equal(2);
      expect(list.getHead().getData()).to.equal("test");
      expect(list.getTail().getData()).to.equal("testTwo");
      expect(list.getHead().getNext().getData()).to.equal("testTwo");
      expect(list.getHead().getNext().getNext().getData()).to.equal("test");
    });

    it("can correctly insert after the last item in a multi-item list", async () => {
      const list = new CircularLinkedList();
      list.pushEnd(1);
      list.pushEnd(2);
      list.pushEnd(3);

      const insertOp = list.insertAfterPrimitive(4, 3);
      expect(insertOp).to.equal(true);
      expect(list.getLength()).to.equal(4);

      const dataList = list.getList();
      expect(dataList[0]).to.equal(1);
      expect(dataList[1]).to.equal(2);
      expect(dataList[2]).to.equal(3);
      expect(dataList[3]).to.equal(4);

      expect(list.getHead().getData()).to.equal(1);
      expect(list.getTail().getData()).to.equal(4);
      expect(list.getTail().getNext().getData()).to.equal(1);
    });

    it("can find and insert before a Node with primitive data", async () => {
      const list = new CircularLinkedList();
      list.pushEnd(2);
      list.pushEnd(17);
      list.pushEnd(22);

      const insertOp = list.insertBeforePrimitive(4, 17);
      expect(insertOp).to.equal(true);
      expect(list.getLength()).to.equal(4);

      const dataList = list.getList();
      expect(dataList[0]).to.equal(2);
      expect(dataList[1]).to.equal(4);
      expect(dataList[2]).to.equal(17);
      expect(dataList[3]).to.equal(22);
    });

    it("can find and insert before the first Node in the list", async () => {
      const list = new CircularLinkedList();
      list.pushEnd(2);
      list.pushEnd(17);
      list.pushEnd(22);

      const insertOp = list.insertBeforePrimitive(1, 2);
      expect(insertOp).to.equal(true);
      expect(list.getLength()).to.equal(4);

      const dataList = list.getList();
      expect(dataList[0]).to.equal(1);
      expect(dataList[1]).to.equal(2);
      expect(dataList[2]).to.equal(17);
      expect(dataList[3]).to.equal(22);
    });

    it("can correctly insert before an item in a single-item list", async () => {
      const list = new CircularLinkedList();
      list.pushEnd("test");

      const insertOp = list.insertBeforePrimitive("testTwo", "test");
      expect(insertOp).to.equal(true);
      expect(list.getLength()).to.equal(2);
      expect(list.getHead().getData()).to.equal("testTwo");
      expect(list.getTail().getData()).to.equal("test");
      expect(list.getHead().getNext().getData()).to.equal("test");
      expect(list.getHead().getNext().getNext().getData()).to.equal("testTwo");
    });

    it("can get all data from the list as an array", async () => {
      const list = new CircularLinkedList();
      list.pushEnd(1);
      list.pushEnd(2);
      list.pushEnd(3);

      expect(list.getList()).to.be.an("array")
        .with.length(3)
        .to.include(1)
        .to.include(2)
        .to.include(3);
    });

    it("can search the list for a value", async () => {
      const list =  new CircularLinkedList();
      list.pushEnd("test1");
      list.pushEnd("test2");
      list.pushEnd("test3");
      list.pushEnd("test4");

      let search: ListSearchResponse = list.search("test4");
      expect(search).to.be.an("object")
        .to.have.all.keys("found", "depth", "node", "prev");
      expect(search.found).to.equal(true);
      expect(search.depth).to.equal(3);

      search = list.search("blarg");
      expect(search).to.be.an("object")
        .to.have.property("found")
        .to.equal(false);
    });

    it("can search the list for an object with given key, value", async () => {
      const list =  new CircularLinkedList();
      const testOne: any = { name: "joe", age: 11 };
      const testTwo: any = { name: "jim", age: 22 };
      const testThree: any = { name: "jak", age: 33 };
      list.pushEnd(testOne);
      list.pushEnd(testTwo);
      list.pushEnd(testThree);

      const search: ListSearchResponse = list.search("joe", "name");
      expect(search).to.be.an("object")
        .to.have.all.keys("found", "depth", "node", "prev");
      expect(search.found).to.equal(true);
      expect(search.depth).to.equal(0);
    });

    it("can find and remove a given value from the list", async () => {
      const list = new CircularLinkedList();
      list.pushEnd(4);
      list.pushEnd(17);
      list.pushEnd(66);
      list.pushEnd(213);
      list.pushEnd(4193);

      expect(list.getLength()).to.equal(5);
      let search = list.searchAndDelete(66);
      expect(search).to.equal(true);
      expect(list.getLength()).to.equal(4);

      search = list.searchAndDelete(213);
      expect(search).to.equal(true);
      expect(list.getLength()).to.equal(3);

      expect(list.getHead().getData()).to.equal(4);
      expect(list.getHead().getNext().getData()).to.equal(17);
      expect(list.getHead().getNext().getNext().getData()).to.equal(4193);
      expect(list.getHead().getNext().getNext().getNext().getData()).to.equal(4);

      search = list.searchAndDelete(999999);
      expect(search).to.equal(false);
    });

    it("can find and delete an object with a given key, value", async () => {
      const list =  new CircularLinkedList();
      const testOne: any = { name: "joe", age: 11 };
      const testTwo: any = { name: "jim", age: 22 };
      const testThree: any = { name: "jak", age: 33 };
      list.pushEnd(testOne);
      list.pushEnd(testTwo);
      list.pushEnd(testThree);
      expect(list.getLength()).to.equal(3);

      let search = list.searchAndDelete("joe", "name");
      expect(search).to.equal(true);
      expect(list.getLength()).to.equal(2);

      search = list.searchAndDelete("jim", "name");
      expect(search).to.equal(true);
      expect(list.getLength()).to.equal(1);
    });

    it("can delete the first item in the list", async () => {
      const list = new CircularLinkedList();
      list.pushEnd(1);
      list.pushEnd(2);
      list.pushEnd(3);
      list.deleteFirst();

      expect(list.getHead().getData()).to.equal(2);
      expect(list.getHead().getNext().getData()).to.equal(3);
      expect(list.getList()).to.be.an("array")
        .with.length(2)
        .to.include(2)
        .to.include(3);
    });

  });

  describe("DoublyLinkedList", () => {
    it("can create a list and add an item to it", async () => {
      const list = new DoublyLinkedList();
      list.pushEnd("test");

      expect(list.getLength()).to.equal(1);
      expect(list.getList()).to.be.an("array")
        .with.length(1);
    });

    it("can create a list and add 5 items to it", async () => {
      const list: DoublyLinkedList = new testList();

      expect(list.getLength()).to.equal(5);
      expect(list.getList()).to.be.an("array")
        .with.length(5);
      expect(list.getHead().getData()).to.equal("test");
      expect(list.getHead().getNext().getData()).to.equal("test2");
      expect(list.getHead().getNext().getNext().getData()).to.equal("test3");
      expect(list.getHead().getNext().getNext().getNext().getData()).to.equal("test4");
      expect(list.getHead().getNext().getNext().getNext().getNext().getData()).to.equal("test5");
    });

    it("can put an item at the front of the list", () => {
      const list: DoublyLinkedList = new testList();
      list.pushFront("test6");

      expect(list.getList()).to.be.an("array").with.length(6);
      expect(list.getHead().getData()).to.equal("test6");
      expect(list.getTail().getData()).to.equal("test5");
    });

    it("can search for a value/key", () => {
      const list: DoublyLinkedList = new DoublyLinkedList();
      list.pushFront({ item: "monkey" });
      list.pushFront({ item: "bear" });
      list.pushFront({ item: "badger" });

      let search: ListSearchResponse = list.search("bear", "item");
      expect(search.node.getData())
        .to.be.an("object")
        .to.deep.equal({ item: "bear" });

      search = list.search("monkey", "item");
      expect(search.node.getData())
        .to.be.an("object")
        .to.deep.equal({ item: "monkey" });

      search = list.search("badger", "item");
      expect(search.node.getData())
        .to.be.an("object")
        .to.deep.equal({ item: "badger" });
    });

    it("can search the list for a value", () => {
      const list: DoublyLinkedList = new DoublyLinkedList();
      list.pushEnd(5);

      const search: ListSearchResponse = list.search(5);
      expect(search.node.getData()).to.equal(5);
    });

    it("returns null if search criteria are not met", () => {
      const list: DoublyLinkedList = new DoublyLinkedList();
      list.pushEnd(5);

      const search: ListSearchResponse = list.search(615);
      expect(search).to.be.an("object")
        .with.all.keys("found");
      expect(search.found).to.equal(false);
    });

    it("can search for and delete a node", () => {
      const list: DoublyLinkedList = new DoublyLinkedList();
      list.pushEnd("red");
      list.pushEnd("orange");
      list.pushEnd("yellow");
      list.pushEnd("green");
      list.pushEnd("blue");
      list.pushEnd("indigo");
      list.pushEnd("violet");
      expect(list.getLength()).to.equal(7);

      list.searchAndDelete("green");
      expect(list.getLength()).to.equal(6);
      let search: ListSearchResponse = list.search("yellow");
      const yellowNode = search.node as Node;
      expect(yellowNode.getNext().getData()).to.equal("blue");

      search = list.search("blue");
      const blueNode = search.node as Node;
      expect(blueNode.getPrev().getData()).to.equal("yellow");
    });

    it("can delete the first item in the list", () => {
      const list: DoublyLinkedList = testList();
      const originalLength = list.getLength();
      const secondItemData = list.getHead().getNext().getData();
      const thirdItemData = list.getHead().getNext().getNext().getData();

      list.deleteFirst();
      expect(list.getLength()).to.equal(originalLength - 1);
      expect(list.getHead().getData()).to.equal(secondItemData);

      list.deleteFirst();
      expect(list.getLength()).to.equal(originalLength - 2);
      expect(list.getHead().getData()).to.equal(thirdItemData);
    });

    it("can traverse the list in reverse order", () => {
      const list: DoublyLinkedList = testList();
      expect(list.getHead().getData()).to.equal("test");
      expect(list.getHead().getNext().getData()).to.equal("test2");
      expect(list.getHead().getNext().getNext().getData()).to.equal("test3");
      expect(list.getHead().getNext().getNext().getNext().getData()).to.equal("test4");
      expect(list.getHead().getNext().getNext().getNext().getNext().getData()).to.equal("test5");

      const reverse = list.getListReverse();
      expect(reverse).to.be.an("array").with.length(5);
      expect(reverse[0]).to.equal("test5");
      expect(reverse[1]).to.equal("test4");
      expect(reverse[2]).to.equal("test3");
      expect(reverse[3]).to.equal("test2");
      expect(reverse[4]).to.equal("test");
    });

  });

  describe("GraphNode", () => {
    it("can create a graph", async () => {
      const g = new GraphNode("test");
      expect(g.getData()).to.equal("test");
      expect(g.getToNodes().length).to.equal(0);
      expect(g.getFromNodes().length).to.equal(0);
    });

    it("can create a graph and connect nodes", async () => {
      const g = new GraphNode("test");
      const newNode = new GraphNode("newnode");

      expect(g.connectTwoWays(newNode)).to.equal(true);

      expect(g.getToNodes().length).to.equal(1);
      expect(g.getToNodes()).to.include(newNode);
      expect(newNode.getFromNodes().length).to.equal(1);
      expect(newNode.getFromNodes()).to.include(g);

      expect(g.isConnectedTo(newNode)).to.equal(true);
      expect(newNode.isConnectedFrom(g)).to.equal(true);
    });

    it("can connect a node to itself", async () => {
      const g = new GraphNode("test");
      g.connectTwoWays(g);

      expect(g.getToNodes()).to.include(g);
      expect(g.getToNodes().length).to.equal(1);
      expect(g.getFromNodes()).to.include(g);
      expect(g.getFromNodes().length).to.equal(1);
    });

    it("will not make a second connection to an already connected node", async () => {
      const g = new GraphNode("test");
      const h = new GraphNode("test");
      expect(g.connectTwoWays(h)).to.equal(true);
      expect(g.connectTwoWays(h)).to.equal(false);
      expect(g.getToNodes().length).to.equal(1);
      expect(h.getFromNodes().length).to.equal(1);
    });

    it("can make a one-way to connection", async () => {
      const g = new GraphNode("test");
      const h = new GraphNode("test");

      expect(g.connectToOneWay(h)).to.equal(true);

      expect(g.getToNodes().length).to.equal(1);
      expect(g.getToNodes()[0]).to.equal(h);
      expect(g.getFromNodes().length).to.equal(0);
      expect(h.getFromNodes().length).to.equal(1);
      expect(h.getFromNodes()[0]).to.equal(g);
      expect(h.getToNodes().length).to.equal(0);
    });

    it("can make a one-way from connection", async () => {
      const g = new GraphNode("test");
      const h = new GraphNode("test");

      expect(h.connectFromOneWay(g)).to.equal(true);

      expect(g.getToNodes().length).to.equal(1);
      expect(g.getToNodes()[0]).to.equal(h);
      expect(h.getToNodes().length).to.equal(0);
      expect(h.getFromNodes().length).to.equal(1);
      expect(h.getFromNodes()[0]).to.equal(g);
    });

    it("can remove a connection to another node", async () => {
      const g = new GraphNode("test");
      const h = new GraphNode("testTwo");
      expect(g.connectTwoWays(h)).to.equal(true);
      expect(g.getToNodes().length).to.equal(1);
      expect(h.getFromNodes().length).to.equal(1);

      expect(g.removeConnectionTo(h)).to.equal(true);
      expect(g.getToNodes().length).to.equal(0);
      expect(h.getFromNodes().length).to.equal(0);
    });

    it("will not remove a to connection that does not exist", async () => {
      const g = new GraphNode("test");
      const h = new GraphNode("testTwo");
      expect(g.connectTwoWays(h)).to.equal(true);
      expect(g.getToNodes().length).to.equal(1);
      expect(h.getFromNodes().length).to.equal(1);

      expect(g.removeConnectionTo(h)).to.equal(true);
      expect(g.getToNodes().length).to.equal(0);
      expect(h.getFromNodes().length).to.equal(0);

      expect(g.removeConnectionTo(h)).to.equal(false);
    });

    it("will remove a one-way to connection", async () => {
      const g = new GraphNode("test");
      const h = new GraphNode("testTwo");
      expect(g.connectToOneWay(h)).to.equal(true);

      expect(g.removeConnectionTo(h)).to.equal(true);
      expect(g.removeConnectionTo(h)).to.equal(false);

      expect(g.getToNodes().length).to.equal(0);
      expect(h.getFromNodes().length).to.equal(0);
    });

    it("will remove a one-way from connection", async () => {
      const g = new GraphNode("test");
      const h = new GraphNode("testTwo");
      expect(g.connectFromOneWay(h)).to.equal(true);
      expect(g.getFromNodes().length).to.equal(1);

      expect(g.removeConnectionFrom(h)).to.equal(true);
      expect(g.removeConnectionFrom(h)).to.equal(false);

      expect(g.getFromNodes().length).to.equal(0);
      expect(h.getToNodes().length).to.equal(0);
    });

    it("can remove connections between nodes", async () => {
      const g = new GraphNode("test");
      const h = new GraphNode("testTwo");
      const i = new GraphNode("testThree");
      g.connectTwoWays(h);
      g.connectTwoWays(i);
      h.connectTwoWays(i);
      expect(g.getFromNodes().length).to.equal(2);

      expect(g.removeConnections(h)).to.equal(true);
      expect(g.getToNodes().length).to.equal(1);

      expect(g.removeConnections(i)).to.equal(true);
      expect(g.getToNodes().length).to.equal(0);

      expect(h.getToNodes().length).to.equal(1);
      expect(h.removeConnections(i)).to.equal(true);
      expect(h.getToNodes().length).to.equal(0);

      expect(i.getToNodes().length).to.equal(0);
      expect(g.getFromNodes().length).to.equal(0);
      expect(h.getFromNodes().length).to.equal(0);
      expect(i.getFromNodes().length).to.equal(0);
    });

  });

  describe("PriorityQueue", () => {
    const pq = new PriorityQueue();
    beforeEach(() => { pq.reset(); });

    it("can create a priority queue and insert items", async () => {
      expect(pq.isEmpty()).to.equal(true);
      pq.load("red", MAX_PRIORITY);
      pq.load("orange", 1);
      pq.load("yellow", 2);

      expect(pq.isEmpty()).to.equal(false);
      expect(pq.length()).to.equal(3);
    });

    it("retrieves items in the correct order", async () => {
      pq.load("red", MAX_PRIORITY);
      pq.load("green", 3);
      pq.load("orange", 1);
      pq.load("blue", 4);
      pq.load("yellow", 2);

      const red = pq.next();
      const orange = pq.next();
      const yellow = pq.next();
      const green = pq.next();
      const blue = pq.next();
      expect(red.item).to.equal("red");
      expect(orange.item).to.equal("orange");
      expect(yellow.item).to.equal("yellow");
      expect(green.item).to.equal("green");
      expect(blue.item).to.equal("blue");
    });

    it("can insert items with the same priority", async () => {
      pq.load("red1", MAX_PRIORITY);
      pq.load("red2", MAX_PRIORITY);
      pq.load("red3", 0);
      pq.load("orange1", 1);
      pq.load("orange2", 1);
      pq.load("orange3", 1);
      pq.load("yellow1", 2);
      pq.load("yellow2", 2);
      pq.load("yellow3", 2);

      expect(pq.next().item).to.equal("red1");
      expect(pq.next().item).to.equal("red2");
      expect(pq.next().item).to.equal("red3");
      expect(pq.next().item).to.equal("orange1");
      expect(pq.next().item).to.equal("orange2");
      expect(pq.next().item).to.equal("orange3");
      expect(pq.next().item).to.equal("yellow1");
      expect(pq.next().item).to.equal("yellow2");
      expect(pq.next().item).to.equal("yellow3");
    });

    it("can merge two priority queues", async () => {
      const q1 = new PriorityQueue();
      q1.load("red", 0);
      q1.load("orange", 1);
      q1.load("yellow", 2);

      const q2 = new PriorityQueue();
      q2.load("red2", 0);
      q2.load("orange2", 1);
      q2.load("yellow2", 2);

      q1.merge(q2);
    });

  });

  describe("Queue", () => {
    const q = new Queue();
    beforeEach(() => { q.reset(); });

    describe(".cutLine()", () => {
      it("can load items at the front of the queue", async () => {
        q.load(4, 5, 6);
        expect(q.length()).to.equal(3);

        q.cutLine(1, 2, 3);
        expect(q.length()).to.equal(6);
        for (let i = 1; i <= 6; i++) {
          expect(q.next()).to.equal(i);
        }
      });
    });

    describe(".load()", () => {
      it("can load a Queue with an object", async () => {
        const lengthAtCreation = q.length();
        q.load({ data: true });
        const lengthAfterLoad = q.length();

        expect(lengthAtCreation).to.equal(0);
        expect(lengthAfterLoad).to.equal(1);
      });

      it("can load a Queue with a false boolean", async () => {
        const lengthAtCreation = q.length();
        q.load(false);
        const lengthAfterLoad = q.length();

        expect(lengthAtCreation).to.equal(0);
        expect(lengthAfterLoad).to.equal(1);
      });

      it("can load a Queue with an empty string", async () => {
        const lengthAtCreation = q.length();
        q.load("");
        const lengthAfterLoad = q.length();

        expect(lengthAtCreation).to.equal(0);
        expect(lengthAfterLoad).to.equal(1);
      });

      it("can load a Queue with number 0", async () => {
        const lengthAtCreation = q.length();
        q.load(0);
        const lengthAfterLoad = q.length();

        expect(lengthAtCreation).to.equal(0);
        expect(lengthAfterLoad).to.equal(1);
      });

      it("can load multiple items at a time", async () => {
        q.load(1, 3, 5, 7, 9);
        expect(q.length()).to.equal(5);

        expect(q.next()).to.equal(1);
        expect(q.next()).to.equal(3);
        expect(q.next()).to.equal(5);
        expect(q.next()).to.equal(7);
        expect(q.next()).to.equal(9);
      });
    });

    describe(".next()", () => {
      it("can cycle through a Queue", async () => {
        const items = 7;
        for (let i = 0; i < items; i++) {
          q.load(i);
        }
        expect(q.length()).to.equal(items);

        for (let i = 0, len = q.length(); i < len; i++) {
          expect(q.next()).to.equal(i)
        }

        expect(q.length()).to.equal(0);
      });
    });

    describe(".unloadAll()", () => {
      it("can unloadAll from a Queue", async () => {
        const items = 10;
        for (let i = 0; i < items; i++) {
          q.load({ data: true });
        }
        expect(q.length()).to.equal(items);

        const dump = q.unloadAll();
        expect(dump.length).to.equal(items);
        expect(q.length()).to.equal(0);
      });
    });

    describe(".view()", () => {
      it("can view an arbitrary item in the queue if it exists", async () => {
        q.load(1,2,3,4,5);

        expect(q.view(0)).to.equal(1);
        expect(q.view(2)).to.equal(3);
        expect(q.view(4)).to.equal(5);
      });

      it("returns null when viewing an item out of range", async () => {
        q.load(true, true, true);
        expect(q.length()).to.equal(3);

        expect(q.view(8)).to.be.a("null");
      });

      it("can view the entire queue", async () => {
        q.load("red", "black", "green", "yellow", "blue");
        expect(q.length()).to.equal(5);

        const view = q.view();
        expect(view.length).to.equal(5);

        view.pop();
        view.pop();
        expect(view[2]).to.equal("green");
        expect(view.length).to.equal(3);
        expect(q.length()).to.equal(5);
      });
    });
  });

  describe("SinglyLinkedList", () => {
    it("can create a linked list and add an item to it", async () => {
      const list = new SinglyLinkedList();
      expect(list.isEmpty()).to.equal(true);
      list.pushEnd("test");
      expect(list.isEmpty()).to.equal(false);

      const currentList = list.getList();
      expect(currentList).to.be.an("array").with.length(1);
      expect(list.getHead().getData()).to.equal("test");
      expect(list.getTail().getData()).to.equal("test");
    });

    it("can create a linked list and add items to it", async () => {
      const list = new SinglyLinkedList();
      list.pushEnd("test");
      list.pushEnd("test2");
      list.pushEnd("test3");
      list.pushEnd("test4");

      const currentList = list.getList();
      expect(currentList).to.be.an("array").with.length(4);

      expect(list.getHead().getData()).to.equal("test");
      expect(list.getHead().getNext().getData()).to.equal("test2");
      expect(list.getHead().getNext().getNext().getData()).to.equal("test3");
      expect(list.getHead().getNext().getNext().getNext().getData()).to.equal("test4");
      expect(list.getTail().getData()).to.equal("test4");
    });

    it("can insert new items at the front of the list", async () => {
      const list = dummyList();
      expect(list.getLength()).to.equal(5);

      list.pushFront({ color: "orange" });
      expect(list.getLength()).to.equal(6);
      expect(list.getHead().getData()).to.be.an("object")
        .to.have.all.keys("color");
    });

    it("can search for an element in a list", async () => {
      const list = dummyList();

      let search = list.search(3);
      let searchData = search.node.getData();
      expect(searchData).to.equal(3);

      search = list.search(true, "wobble");
      searchData = search.node.getData();
      expect(searchData).to.be.an("object")
        .to.have.all.keys("test", "wobble");
    });

    it("can find and delete an element in a list", async () => {
      const list = dummyList();
      expect(list.getLength()).to.equal(5);

      list.searchAndDelete(3);
      expect(list.getLength()).to.equal(4);

      list.searchAndDelete(true, "wobble");
      expect(list.getLength()).to.equal(3);
    });

    it("can delete the first element in a list", async () => {
      const list = dummyList();
      expect(list.getLength()).to.equal(5);
      expect(list.getHead().getData()).to.equal(3);

      list.deleteFirst();
      expect(list.getLength()).to.equal(4);
      expect(list.getHead().getData()).to.equal("fifteen");
    });

  });

  describe("Stack", () => {
    const MAX_SIZE = 5;
    const stk = new Stack(MAX_SIZE);

    beforeEach(() => {
      stk.clear();
    });

    describe("put(), get()", () => {
      it("can put and get 5 items", async () => {
        for (let i = 1; i <= MAX_SIZE; i++) {
          stk.put(`item${i}`);
        }

        expect(stk.get()).to.equal("item5");
        expect(stk.get()).to.equal("item4");
        expect(stk.get()).to.equal("item3");
        expect(stk.get()).to.equal("item2");
        expect(stk.get()).to.equal("item1");
      });

      it("can peek at the top element of the stack", async () => {
        for (let i = 1; i <= MAX_SIZE; i++) {
          stk.put(`item${i}`);
        }
        expect(stk.peek()).to.equal("item5");
        expect(stk.get()).to.equal("item5");
        expect(stk.peek()).to.equal("item4");
      });

      it("can check the stack size", async () => {
        for (let i = 1; i <= MAX_SIZE; i++) {
          stk.put(`item${i}`);
        }

        expect(stk.getStackSize()).to.equal(MAX_SIZE);
      });

      it("can return the whole stack", async () => {
        for (let i = 1; i <= MAX_SIZE; i++) {
          stk.put(`item${i}`);
        }

        expect(stk.getStack()).to.be.an("array").with.length(5);
      });

      it("creates a stack of size 1 if given a bad max_size", async () => {
        const tmp = new Stack(-1);
        expect(tmp.getMaxSize()).to.equal(1);

        const tmpTwo = new Stack("cool");
        expect(tmpTwo.getMaxSize()).to.equal(1);
      });

      it("can tell if the stack is empty", async () => {
        expect(stk.isEmpty()).to.equal(true);

        stk.put("item1");
        expect(stk.isEmpty()).to.equal(false);
      });

      it("can tell if the stack is full", async () => {
        for (let i = 1; i <= MAX_SIZE; i++) {
          stk.put(`item${i}`);
          if (i !== MAX_SIZE) {
            expect(stk.isFull()).to.equal(false);
          }
        }
        expect(stk.isFull()).to.equal(true);
      });
    });

  });

});

function dummyList(): SinglyLinkedList {
  const list = new SinglyLinkedList();
  list.pushEnd(3);
  list.pushEnd("fifteen");
  list.pushEnd(false);
  list.pushEnd(41);
  list.pushEnd({
    test: "this",
    wobble: true,
  });
  return list;
}

function testList(): DoublyLinkedList {
  const list = new DoublyLinkedList();
  list.pushEnd("test");
  list.pushEnd("test2");
  list.pushEnd("test3");
  list.pushEnd("test4");
  list.pushEnd("test5");
  return list;
}
