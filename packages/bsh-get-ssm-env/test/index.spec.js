const fs = require('fs');

const awsParamStore = require('aws-param-store');
const { expect } = require('chai');
const sinon = require('sinon');

const { getParams } = require('../src');

const DEFAULT_ENV_FILE = '.env';
const DEFAULT_PREFIX = '/';
const DEFAULT_REGION = 'us-east-2';

describe('getParams()', () => {
  describe('validation', () => {
    let sandbox;
    let awsParamsStub;
    let fsExistsStub;
    let fsWriteStub;

    beforeEach(() => {
      sandbox = sinon.createSandbox();
      awsParamsStub = sandbox.stub(awsParamStore, 'getParametersByPathSync').returns([]);
      fsExistsStub = sandbox.stub(fs, 'existsSync');
      fsWriteStub = sandbox.stub(fs, 'writeFileSync');
    });
    afterEach(() => {
      sandbox.restore();
    });

    it('throws an error if prefix is not provided', (done) => {
      try {
        getParams(undefined, DEFAULT_REGION, DEFAULT_ENV_FILE);
        expect.fail();
      } catch (e) {
        expect(e.message).to.equal('prefix is required');
        sinon.assert.callCount(awsParamsStub, 0);
        sinon.assert.callCount(fsExistsStub, 0);
        sinon.assert.callCount(fsWriteStub, 0);
        done();
      }
    });

    it('throws an error if prefix is not a string', (done) => {
      try {
        getParams({}, DEFAULT_REGION, DEFAULT_ENV_FILE);
        expect.fail();
      } catch (e) {
        expect(e.message).to.equal('prefix must be a string');
        sinon.assert.callCount(awsParamsStub, 0);
        sinon.assert.callCount(fsExistsStub, 0);
        sinon.assert.callCount(fsWriteStub, 0);
        done();
      }
    });

    it('throws an error if region is not provided', (done) => {
      try {
        getParams(DEFAULT_PREFIX, undefined, DEFAULT_ENV_FILE);
        expect.fail();
      } catch (e) {
        expect(e.message).to.equal('region is required');
        sinon.assert.callCount(awsParamsStub, 0);
        sinon.assert.callCount(fsExistsStub, 0);
        sinon.assert.callCount(fsWriteStub, 0);
        done();
      }
    });

    it('throws an error if region is not a string', (done) => {
      try {
        getParams(DEFAULT_PREFIX, {}, DEFAULT_ENV_FILE);
        expect.fail();
      } catch (e) {
        expect(e.message).to.equal('region must be a string');
        sinon.assert.callCount(awsParamsStub, 0);
        sinon.assert.callCount(fsExistsStub, 0);
        sinon.assert.callCount(fsWriteStub, 0);
        done();
      }
    });

    it('throws an error if envFile is null', (done) => {
      try {
        getParams(DEFAULT_PREFIX, DEFAULT_REGION, null);
        expect.fail();
      } catch (e) {
        expect(e.message).to.equal('envFile is required');
        sinon.assert.callCount(awsParamsStub, 0);
        sinon.assert.callCount(fsExistsStub, 0);
        sinon.assert.callCount(fsWriteStub, 0);
        done();
      }
    });

    it('default to ".env" if envFile is not provided', (done) => {
      getParams(DEFAULT_PREFIX, DEFAULT_REGION);
      sinon.assert.callCount(awsParamsStub, 1);
      sinon.assert.callCount(fsExistsStub, 1);
      sinon.assert.callCount(fsWriteStub, 1);
      sinon.assert.calledWith(fsWriteStub, DEFAULT_ENV_FILE, '');
      done();
    });

    it('throws an error if envFile is not a string', (done) => {
      try {
        getParams(DEFAULT_PREFIX, DEFAULT_REGION, {});
        expect.fail();
      } catch (e) {
        expect(e.message).to.equal('envFile must be a string');
        sinon.assert.callCount(awsParamsStub, 0);
        sinon.assert.callCount(fsExistsStub, 0);
        sinon.assert.callCount(fsWriteStub, 0);
        done();
      }
    });
  });

  describe('new env file name', () => {
    let sandbox;
    let awsParamsStub;

    beforeEach(() => {
      sandbox = sinon.createSandbox();
      awsParamsStub = sandbox.stub(awsParamStore, 'getParametersByPathSync').returns([]);
    });
    afterEach(() => {
      sandbox.restore();
    });

    it('writes to specified envFile if it does not exist', (done) => {
      const fsExistsStub = sandbox.stub(fs, 'existsSync').returns(false);
      const fsWriteStub = sandbox.stub(fs, 'writeFileSync');

      getParams(DEFAULT_PREFIX, DEFAULT_REGION, DEFAULT_ENV_FILE);

      sinon.assert.calledOnce(fsExistsStub);
      sinon.assert.calledOnce(awsParamsStub);
      sinon.assert.calledWith(fsWriteStub, DEFAULT_ENV_FILE, '');
      done();
    });

    it('writes to specified envFile appended with timestamp if envFile already exists', (done) => {
      const fsExistsStub = sandbox.stub(fs, 'existsSync').returns(true);
      const fsWriteStub = sandbox.stub(fs, 'writeFileSync');

      getParams(DEFAULT_PREFIX, DEFAULT_REGION, DEFAULT_ENV_FILE);

      sinon.assert.calledOnce(fsExistsStub);
      sinon.assert.calledOnce(fsWriteStub);

      const args = fsWriteStub.getCall(0).args;
      const split = args[0].split('-');
      expect(split[0]).to.equal(DEFAULT_ENV_FILE);
      expect(+split[1]).to.be.a('number');
      expect(args[1]).to.equal('');
      done();
    });
  });

  describe('awsParamStore response', () => {
    let sandbox;
    let awsParamsStub;
    let fsExistsStub;
    let fsWriteStub;

    const testParam = { Name: 'Test', Value: 'xyz123' };

    beforeEach(() => {
      sandbox = sinon.createSandbox();
      awsParamsStub = sandbox.stub(awsParamStore, 'getParametersByPathSync').returns([ testParam ]);
      fsExistsStub = sandbox.stub(fs, 'existsSync');
      fsWriteStub = sandbox.stub(fs, 'writeFileSync');
    });
    afterEach(() => {
      sandbox.restore();
    });

    it('parses and writes returned parameters', (done) => {
      getParams(DEFAULT_PREFIX, DEFAULT_REGION, DEFAULT_ENV_FILE);

      const text = `${testParam.Name}=${testParam.Value}\n`;
      sinon.assert.calledWith(fsWriteStub, DEFAULT_ENV_FILE, text);
      done();
    });
  });
});
