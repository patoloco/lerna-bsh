const fs = require('fs');

const awsParamStore = require('aws-param-store');

function getParams(prefix, region, envFile = '.env') {
  if (!prefix) {
    throw new Error('prefix is required');
  } else if (!region) {
    throw new Error('region is required');
  } else if (!envFile) {
    throw new Error('envFile is required');
  } else if (typeof prefix !== 'string') {
    throw new Error('prefix must be a string');
  } else if (typeof region !== 'string') {
    throw new Error('region must be a string');
  } else if (typeof envFile !== 'string') {
    throw new Error('envFile must be a string');
  }

  let newEnvFile = envFile;
  if (fs.existsSync(`${envFile}`)) {
    const now = new Date().getTime();
    newEnvFile += `-${now}`;
  }

  const parameters = awsParamStore.getParametersByPathSync(prefix, { region });
  let text = '';
  parameters.forEach((param) => {
    text += `${param.Name}=${param.Value}\n`;
  });
  fs.writeFileSync(newEnvFile, text);
}

module.exports = {
  getParams,
};
