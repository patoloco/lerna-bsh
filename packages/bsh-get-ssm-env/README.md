#### Utility to create/update .env file from AWS SSM

##### If .env does not exist it will be created
##### If .env exists a .env-timestamp file will be created
##### A parameter prefix and AWS region must be provided
##### A different file name can optionally be provided

##### Examples
```
const { getParams } = require('bsh-get-ssm-env');

const PREFIX = '/dev';
const REGION = 'us-east-2';

getParams(PREFIX, REGION);
```

```
const PREFIX = '/dev';
const REGION = 'us-east-2';
const ENV_FILE = '.environment';

getParams(PREFIX, REGION, ENV_FILE);
```
