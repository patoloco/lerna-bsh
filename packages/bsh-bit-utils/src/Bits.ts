import { binStrToUint8, isBitSet, setBit, uint8ToBinStr, unsetBit } from "./utils";

export const SIZE_BYTES = 8;

export const MAX_INT = 255;
export const MIN_INT = 0;

export class Bits {
    public readonly size = SIZE_BYTES;
    private buf: ArrayBuffer;
    private views: Map<number, DataView>;

    constructor() {
        this.size = SIZE_BYTES;
        this.buf = new ArrayBuffer(this.size);
        this.views = new Map<number, DataView>();

        this.init();
    }

    public clearAll(): boolean {
        let cleared = 0;
        for (let i = 0; i < this.size; i++) {
            const v = this.views.get(i);
            if (v) {
                v.setUint8(0, 0);
                cleared++;
            }
        }
        return cleared === this.size;
    }

    public getAllString(): string[] {
        const bits: any[] = [];
        for (let i = 0; i < this.size; i++) {
            const v = this.views.get(i);
            if (v) {
                const tmp = uint8ToBinStr(v.getUint8(0));
                bits.push(tmp);
            }
        }
        return bits;
    }

    public getAllDecimal(): number[] {
        const bits: number[] = [];
        for (let i = 0; i < this.size; i++) {
            const v = this.views.get(i);
            if (v) {
                const tmp = v.getUint8(0);
                bits.push(tmp);
            }
        }
        return bits;
    }

    public getRegisterDecimal(reg: number): any {
        if (reg < 0 || reg > 7) {
            return null;
        }
        const tmp = this.views.get(reg);
        return tmp ? tmp.getUint8(0) : null;
    }

    public getRegisterString(reg: number): any {
        if (reg < 0 || reg > 7) {
            return null;
        }
        const tmp = this.views.get(reg);
        return tmp ? uint8ToBinStr(tmp.getUint8(0)) : null;
    }

    public setRegister(reg: number, x: number | string): boolean | null {
        if (reg < 0 || reg > 7) {
            return false;
        }

        let dec: any;
        if (typeof(x) === "string") {
            if (x.length !== 8) {
                return false;
            }
            const bin = binStrToUint8(x);
            if (bin < 0 || bin > 255) {
                return false;
            }
            dec = bin;
        } else if (typeof(x) === "number") {
            if (x < 0 || x > 255) {
                return false;
            }
            dec = x;
        }

        if (typeof(dec) === "number") {
            const v = this.views.get(reg);
            if (v) {
                v.setUint8(0, dec);
                return true;
            }
        }

        return null;
    }

    public isBitSet(reg: number, bit: number): boolean | null {
        const register = this.getRegisterDecimal(reg);
        return isBitSet(register, bit);
    }

    public setBit(reg: number, bit: number): boolean {
        const register = this.getRegisterDecimal(reg);
        const newBit = setBit(register, bit);
        if (newBit) {
            this.setRegister(reg, newBit);
            return true;
        }
        return false;
    }

    public unsetBit(reg: number, bit: number): boolean {
        const register = this.getRegisterDecimal(reg);
        const newBit = unsetBit(register, bit);
        if (newBit) {
            this.setRegister(reg, newBit);
            return true;
        }
        return false;
    }

    private init() {
        for (let i = 0; i < this.size; i++) {
            const view = new DataView(this.buf, i, 1);
            this.views.set(i, view);
        }

        this.clearAll();
    }

}
