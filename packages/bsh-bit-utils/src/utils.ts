export function uint8ToBinStr(x: number): null | string {
    if (x < 0 || x > 255) {
        return null;
    }

    let str = "";
    for (let i = 7; i >= 0; i--) {
        const diff = x - (2 ** i);
        if (diff >= 0) {
            str += "1";
            x = diff;
        } else {
            str += "0";
        }

    }
    return str;
}

export function binStrToUint8(x: string): number {
    const tmp = x.split("");
    let num = 0;
    const len = tmp.length;
    for (let i = 0; i < len; i++) {
        const index = len - 1 - i;
        num += +tmp[index] * (2 ** i);
    }
    return num;
}

export function isBitSet(x: number, bit: number): null | boolean {
    if (bit < 0 || bit > 7 || x < 0 || x > 255) {
        return null;
    }

    return (x & (1 << bit)) !== 0;
}

export function setBit(x: number, bit: number): null | number {
    if (isBitSet(x, bit)) {
        return x;
    }

    return x + (1 << bit);
}

export function unsetBit(x: number, bit: number): null | number {
    if (!isBitSet(x, bit)) {
        return x;
    }

    return x - (1 << bit);
}
