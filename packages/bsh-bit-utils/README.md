#Summary
    A class to manage 8 byte ArrayBuffers and some associated utilites for working with bits

    import { Bits } from "bsh-bit-utils";
    const bits = new Bits();
    bits.setBit(3, 2);
    bits.setBit(3, 3);
    bits.setBit(2, 7);

    const decimalBits = bits.getAllDecimal();
