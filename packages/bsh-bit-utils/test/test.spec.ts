import * as chai from "chai";
import * as chaiAsPromised from "chai-as-promised";
import "mocha";

import { Bits, SIZE_BYTES as SIZE, uint8ToBinStr } from "../src";

chai.use(chaiAsPromised);
const expect = chai.expect;
chai.should();

describe("Bit Utilities", () => {
    describe("Bits", () => {
        const bits = new Bits();

        beforeEach(() => {
            bits.clearAll();
        });

        it("can get all the bits as binary strings", async () => {
            const res = bits.getAllString();
            expect(res).to.be.an("array").with.length(SIZE);
        });

        it("can get all the bits as decimal numbers", async () => {
            const res = bits.getAllDecimal();
            expect(res).to.be.an("array").with.length(SIZE);
        });

        it("can set all bits to 0", async () => {
            bits.setRegister(0, 33);
            bits.setRegister(1, 39);
            bits.setRegister(2, 250);
            bits.setRegister(3, 255);
            bits.setRegister(4, 256);
            bits.setRegister(5, 33);
            bits.setRegister(6, 1);
            bits.setRegister(7, "0101111");

            bits.clearAll();
            const res = bits.getAllString();
            expect(res).to.be.an("array").with.length(SIZE);
            for (let i = 0; i < SIZE; i++) {
                expect(+res[ i ]).to.equal(0);
            }
        });

        it("can get the bits of a single register", async () => {
            const x = bits.getRegisterString(3);
            const y = bits.getRegisterDecimal(3);
        });

        it("can check if a specific bit is set in a given register", async () => {
            bits.setRegister(0, "00010001");
            expect(bits.isBitSet(0, 0)).to.equal(true);
            expect(bits.isBitSet(0, 1)).to.equal(false);
            expect(bits.isBitSet(0, 2)).to.equal(false);
            expect(bits.isBitSet(0, 3)).to.equal(false);
            expect(bits.isBitSet(0, 4)).to.equal(true);
            expect(bits.isBitSet(0, 5)).to.equal(false);
            expect(bits.isBitSet(0, 6)).to.equal(false);
            expect(bits.isBitSet(0, 7)).to.equal(false);
        });

        it("can set a specific bit in a given register", async () => {
            bits.setRegister(0, "00010001");
            bits.setBit(0, 2);
            expect(bits.isBitSet(0, 0)).to.equal(true);
            expect(bits.isBitSet(0, 1)).to.equal(false);
            expect(bits.isBitSet(0, 2)).to.equal(true);
            expect(bits.isBitSet(0, 3)).to.equal(false);
            expect(bits.isBitSet(0, 4)).to.equal(true);
            expect(bits.isBitSet(0, 5)).to.equal(false);
            expect(bits.isBitSet(0, 6)).to.equal(false);
            expect(bits.isBitSet(0, 7)).to.equal(false);
        });

        it("can unset a specific bit in a given register", async () => {
            bits.setRegister(0, "00010101");
            expect(bits.isBitSet(0, 2)).to.equal(true);

            bits.unsetBit(0, 2);
            expect(bits.isBitSet(0, 0)).to.equal(true);
            expect(bits.isBitSet(0, 1)).to.equal(false);
            expect(bits.isBitSet(0, 2)).to.equal(false);
            expect(bits.isBitSet(0, 3)).to.equal(false);
            expect(bits.isBitSet(0, 4)).to.equal(true);
            expect(bits.isBitSet(0, 5)).to.equal(false);
            expect(bits.isBitSet(0, 6)).to.equal(false);
            expect(bits.isBitSet(0, 7)).to.equal(false);
        });

    });

});
