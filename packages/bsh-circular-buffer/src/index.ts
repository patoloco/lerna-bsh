import { CircularBufferOptions } from "./models";
export * from "./models";

// import Timer = NodeJS.Timer;

export class CircularBuffer<T> {
  private buffer: T[];
  private interval: number;
  private iter: any;
  private iterator: () => T;
  private size: number;

  constructor(options: CircularBufferOptions<T>) {
    this.buffer = [];
    this.interval = options.interval;
    this.iterator = options.iterator;
    this.size = Math.floor(options.size);
  }

  public clear(): void {
    clearInterval(this.iter);
    this.buffer = [];
  }

  public restart(): void {
    this.buffer = [];
  }

  public start(): void {
    this.iter = setInterval(() => {
      const newVal: T = this.iterator();
      this.buffer.unshift(newVal);
      if (this.buffer.length > this.size) {
        this.buffer.pop();
      }
    }, this.interval);
  }

  public stop(): void {
    clearInterval(this.iter);
  }

  public values(): T[] {
    return this.buffer;
  }

}
