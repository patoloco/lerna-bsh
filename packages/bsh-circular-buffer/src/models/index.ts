export interface CircularBufferOptions<T> {
  interval: number;
  iterator: () => T;
  size: number;
}
