import log4js = require("log4js");
const defaultLogger = log4js.getLogger("default");
defaultLogger.level = "DEBUG";

export const log = defaultLogger;
