#Summary
A basic **Circular Buffer** implementation

    import { CircularBuffer, CircularBufferOptions } from "bsh-circular-buffer";

    const numberIterator = () => {
      return Math.floor(Math.random() * 1000);
    };

    const options: CircularBufferOptions<number> = {
      interval: 250,
      iterator: numberIterator,
      size: 8,
    };

    const buf = new CircularBuffer(options);
    buf.start();

    const timer = setInterval(() => {
      const vals = buf.values();
      console.log("vals:", vals);
    }, 500);

    setTimeout(() => {
      buf.stop();
      clearInterval(timer);
    }, 4000);
