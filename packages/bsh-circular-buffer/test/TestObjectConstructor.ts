export type TestObjectType = {
  name: string;
  age: number;
  goats: boolean;
};

export const TestObjectConstructor = () => {
  return {
    age: Math.floor(Math.random() * 100),
    goats: Math.random() > 0.5,
    name: ["John", "Jane", "Jim", "Joan"][Math.floor(Math.random() * 4)],
  } as TestObjectType;
};
