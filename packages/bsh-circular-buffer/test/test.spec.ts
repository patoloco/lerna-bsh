import { log } from "../src/utils";

import * as chai from "chai";
import * as chaiAsPromised from "chai-as-promised";
import "mocha";

chai.use(chaiAsPromised);
const expect = chai.expect;
chai.should();

import { wait } from "bsh-async-utils";

import { CircularBuffer } from "../src";
import { CircularBufferOptions } from "../src/models";

type StringType = string;
const stringIterator = () => {
  return Math.random().toString(36).replace(/[^a-z]+/g, '').substr(0, 5) as StringType;
};

type NumberType = number;
const numberIterator = () => {
  const digits = Math.floor(Math.random() * 5);
  return Math.floor(Math.random() * (10 ** digits));
};

import { TestObjectType, TestObjectConstructor } from "./TestObjectConstructor";

describe("Circular Buffer", () => {
  describe("works with strings", () => {
    let vals1: StringType[], vals2: StringType[];
    let stringBuf: CircularBuffer<StringType>;

    before(() => {
      const options: CircularBufferOptions<StringType> = {
        interval: 100,
        iterator: stringIterator,
        size: 8,
      };
      stringBuf = new CircularBuffer(options);
      stringBuf.start();
    });

    after(() => {
      stringBuf.clear();
    });

    it("after 420ms it has 4 items", async () => {
      await wait(420);
      vals1 = stringBuf.values().slice();
      expect(vals1).to.be.an("array").with.length(4);
    });

    it("after 840ms it has 8 items", async () => {
      await wait(420);
      vals2 = stringBuf.values().slice();
      expect(vals2).to.be.an("array").with.length(8);
      expect(vals1[2]).to.equal(vals2[6]);
      expect(vals1[3]).to.equal(vals2[7]);
    });
  });

  describe("works with numbers", () => {
    let vals1: NumberType[], vals2: NumberType[];
    let numberBuf: CircularBuffer<NumberType>;

    before(() => {
      const options: CircularBufferOptions<NumberType> = {
        interval: 100,
        iterator: numberIterator,
        size: 8,
      };
      numberBuf = new CircularBuffer(options);
      numberBuf.start();
    });

    after(() => {
      numberBuf.clear();
    });

    it("after 420ms it has 4 items", async () => {
      await wait(420);
      vals1 = numberBuf.values().slice();
      expect(vals1).to.be.an("array").with.length(4);
    });

    it("after 840ms it has 8 items", async () => {
      await wait(420);
      vals2 = numberBuf.values().slice();
      expect(vals2).to.be.an("array").with.length(8);
      expect(vals1[2]).to.equal(vals2[6]);
      expect(vals1[3]).to.equal(vals2[7]);
    });
  });

  describe("works with more complex objects", () => {
    let vals1: TestObjectType[], vals2: TestObjectType[];
    let objBuf: CircularBuffer<TestObjectType>;

    before(() => {
      const options: CircularBufferOptions<TestObjectType> = {
        interval: 100,
        iterator: TestObjectConstructor,
        size: 8,
      };
      objBuf = new CircularBuffer(options);
      objBuf.start();
    });

    after(() => {
      objBuf.clear();
    });

    it("after 420ms it has 4 items", async () => {
      await wait(420);
      vals1 = objBuf.values().slice();
      expect(vals1).to.be.an("array").with.length(4);
    });

    it("after 840ms it has 8 items", async () => {
      await wait(420);
      vals2 = objBuf.values().slice();
      expect(vals2).to.be.an("array").with.length(8);
      expect(vals1[2]).to.equal(vals2[6]);
      expect(vals1[3]).to.equal(vals2[7]);
    });
  });

});
