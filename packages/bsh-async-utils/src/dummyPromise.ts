export function dummyPromise(x: any): Promise<any> {
  return new Promise((resolve, reject) => {
    resolve(x);
  });
}
