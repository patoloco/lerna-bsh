const isPlainObject = require('is-plain-object');

const { messages } = require('./errors');
const http = require('./http');

class UrlList {
  constructor(urls) {
    if (!urls) {
      throw new Error(messages.constructor.noOptions);
    } else if (!isPlainObject(urls)) {
      throw new Error(messages.constructor.notObject);
    } else if (Object.keys(urls).length < 1) {
      throw new Error(messages.constructor.noUrls);
    }

    this.urls = {};
    Object.keys(urls).forEach((url) => {
      this.urls[url] = new Url(urls[url]); // eslint-disable-line
    });

    this.headers = null;
  }

  getUrls() {
    return this.urls;
  }

  setDefaultHeaders(headers) {
    if (!headers) {
      this.headers = null;
      return;
    }

    if (!isPlainObject(headers)) {
      throw new Error(messages.headers.notObject);
    }

    Object.keys(this.urls).forEach((url) => {
      this.urls[url].setDefaultHeaders(headers);
    });
  }
}

class Url {
  constructor(prefix) {
    if (!prefix || typeof prefix !== 'string') {
      throw new Error(messages.url.notString);
    }

    this.prefix = prefix;
    this.headers = null;
  }

  setDefaultHeaders(headers) {
    if (!headers) {
      this.headers = null;
      return;
    }

    if (!isPlainObject(headers)) {
      throw new Error(messages.headers.notObject);
    }

    this.headers = headers;
  }

  async get(path, headers = {}) {
    if (!path) {
      throw new Error(messages.http.noPath);
    } else if (!isPlainObject(headers)) {
      throw new Error(messages.http.badHeaders);
    }

    const options = { headers: {} };
    if (this.headers) {
      // assign default headers from class
      options.headers = this.headers;
    }

    if (Object.keys(headers).length > 0) {
      // assign specific headers from call
      options.headers = Object.assign(options.headers, headers);
    }

    const url = this.prefix + path;

    return await http.get(url, options);
  }

  async post(path, body = {}, headers = {}) {
    if (!path) {
      throw new Error(messages.http.noPath);
    } else if (!isPlainObject(headers)) {
      throw new Error(messages.http.badHeaders);
    }

    const options = { body, headers: {} };
    if (this.headers) {
      // assign default headers from class
      options.headers = this.headers;
    }

    if (Object.keys(headers).length > 0) {
      // assign specific headers from call
      options.headers = Object.assign(options.headers, headers);
    }

    const url = this.prefix + path;

    return await http.post(url, options);
  }

  async put(path, body = {}, headers = {}) {
    if (!path) {
      throw new Error(messages.http.noPath);
    } else if (!isPlainObject(headers)) {
      throw new Error(messages.http.badHeaders);
    }

    const options = { body, headers: {} };
    if (this.headers) {
      // assign default headers from class
      options.headers = this.headers;
    }

    if (Object.keys(headers).length > 0) {
      // assign specific headers from call
      options.headers = Object.assign(options.headers, headers);
    }

    const url = this.prefix + path;

    return await http.put(url, options);
  }

  async delete(path, headers = {}) {
    if (!path) {
      throw new Error(messages.http.noPath);
    } else if (!isPlainObject(headers)) {
      throw new Error(messages.http.badHeaders);
    }

    const options = { headers: {} };
    if (this.headers) {
      // assign default headers from class
      options.headers = this.headers;
    }

    if (Object.keys(headers).length > 0) {
      // assign specific headers from call
      options.headers = Object.assign(options.headers, headers);
    }

    const url = this.prefix + path;

    return await http.delete(url, options);
  }
}

module.exports = {
  UrlList,
  Url,
};
