const messages = {
  constructor: {
    invalidDefault: 'default prefix not found in prefix list',
    noDefault: 'default prefix must be provided',
    noOptions: 'constructor options must be provided',
    notObject: 'Options must be an object',
    noPrefixes: 'Must provide at least one prefix',
    noUrls: 'Must provide at least one URL',
  },
  headers: {
    notObject: 'Headers must be provided as an object',
  },
  http: {
    badHeaders: 'invalid headers',
    failure: 'HTTP error',
    noPath: 'URL path must be provided',
    noPrefix: 'No URL prefix has been specified',
  },
  setPrefix: {
    notFound: 'Specified prefix not found',
  },
  url: {
    notString: 'URL prefix must be a string',
  },
};

module.exports = {
  messages,
};
