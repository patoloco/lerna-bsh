/* eslint-disable no-unused-vars */
const request = require('request');

function get(url, options) {
  const reqOptions = {
    headers: options.headers,
    method: 'GET',
    url,
    json: {},
  };

  return new Promise((resolve, reject) => {
    request.get(reqOptions, (err, response, body) => {
      if (err) {
        reject(err);
      } else {
        resolve(response);
      }
    });
  });
}

function post(url, options) {
  const reqOptions = {
    headers: options.headers,
    json: options.body,
    method: 'POST',
    url,
  };

  return new Promise((resolve, reject) => {
    request.post(reqOptions, (err, response, body) => {
      if (err) {
        reject(err);
      } else {
        resolve(response);
      }
    });
  });
}

function put(url, options) {
  const reqOptions = {
    headers: options.headers,
    json: options.body,
    method: 'PUT',
    url,
  };

  return new Promise((resolve, reject) => {
    request.put(reqOptions, (err, response, body) => {
      if (err) {
        reject(err);
      } else {
        resolve(response);
      }
    });
  });
}


function del(url, options) {
  const reqOptions = {
    headers: options.headers,
    method: 'DELETE',
    url,
    json: {},
  };

  return new Promise((resolve, reject) => {
    request.delete(reqOptions, (err, response, body) => {
      if (err) {
        reject(err);
      } else {
        resolve(response);
      }
    });
  });
}

module.exports = {
  delete: del,
  get,
  post,
  put,
};
