const isPlainObject = require('is-plain-object');

const { messages } = require('./errors');
const http = require('./http');

class UrlListByStage {
  constructor(options) {
    if (!options) {
      throw new Error(messages.constructor.noOptions);
    } else if (!isPlainObject(options)) {
      throw new Error(messages.constructor.notObject);
    } else if (!options.prefixes || Object.keys(options.prefixes).length < 1) {
      throw new Error(messages.constructor.noPrefixes);
    } else if (!options.defaultPrefix) {
      throw new Error(messages.constructor.noDefault);
    } else if (!Object.keys(options.prefixes).includes(options.defaultPrefix)) {
      throw new Error(messages.constructor.invalidDefault);
    }

    this.prefixes = new Map();
    Object.keys(options.prefixes).forEach((prefix) => {
      this.prefixes.set(prefix, options.prefixes[prefix]);
    });

    this.prefix = options.defaultPrefix;
    this.headers = null;
  }

  getPrefix() {
    return this.prefix;
  }

  getPrefixes() {
    return this.prefixes;
  }

  setPrefix(prefix) {
    if (this.prefixes.get(prefix)) {
      this.prefix = prefix;
    } else {
      throw new Error(messages.setPrefix.notFound);
    }
  }

  setDefaultHeaders(headers) {
    if (!headers) {
      this.headers = null;
      return;
    }
    if (!isPlainObject(headers)) {
      throw new Error(messages.headers.notObject);
    }
    this.headers = headers;
  }

  async get(path, headers = {}) {
    if (!path) {
      throw new Error(messages.http.noPath);
    } else if (!isPlainObject(headers)) {
      throw new Error(messages.http.badHeaders);
    }

    const options = { headers: {} };
    if (this.headers) {
      // assign default headers from class
      options.headers = this.headers;
    }

    if (Object.keys(headers).length > 0) {
      // assign specific headers from call
      options.headers = Object.assign(options.headers, headers);
    }

    const url = this.prefixes.get(this.prefix) + path;
    return await http.get(url, options);
  }

  async post(path, body = {}, headers = {}) {
    if (!path) {
      throw new Error(messages.http.noPath);
    } else if (!isPlainObject(headers)) {
      throw new Error(messages.http.badHeaders);
    }

    const options = { body, headers: {} };
    if (this.headers) {
      // assign default headers from class
      options.headers = this.headers;
    }

    if (Object.keys(headers).length > 0) {
      // assign specific headers from call
      options.headers = Object.assign(options.headers, headers);
    }

    const url = this.prefixes.get(this.prefix) + path;
    return await http.post(url, options);
  }

  async put(path, body = {}, headers = {}) {
    if (!path) {
      throw new Error(messages.http.noPath);
    } else if (!isPlainObject(headers)) {
      throw new Error(messages.http.badHeaders);
    }

    const options = { body, headers: {} };
    if (this.headers) {
      // assign default headers from class
      options.headers = this.headers;
    }

    if (Object.keys(headers).length > 0) {
      // assign specific headers from call
      options.headers = Object.assign(options.headers, headers);
    }

    const url = this.prefixes.get(this.prefix) + path;
    return await http.put(url, options);
  }

  async delete(path, headers = {}) {
    if (!path) {
      throw new Error(messages.http.noPath);
    } else if (!isPlainObject(headers)) {
      throw new Error(messages.http.badHeaders);
    }

    const options = { headers: {} };
    if (this.headers) {
      // assign default headers from class
      options.headers = this.headers;
    }

    if (Object.keys(headers).length > 0) {
      // assign specific headers from call
      options.headers = Object.assign(options.headers, headers);
    }

    const url = this.prefixes.get(this.prefix) + path;
    return await http.delete(url, options);
  }
}

module.exports = {
  UrlListByStage,
};
