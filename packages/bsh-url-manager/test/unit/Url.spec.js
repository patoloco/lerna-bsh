const { expect } = require('chai');

const { errorMessages, Url } = require('../../src');

describe('Unit: Url', () => {
  describe('constructor', () => {
    it('throws an error if parameter is not a string', (done) => {
      try {
        const url = new Url({});
        expect.fail();
      } catch (e) {
        expect(e.message).to.equal(errorMessages.url.notString);
        done();
      }
    });
  });

  describe('headers', () => {
    it('can set defaultHeaders to null', (done) => {
      const headers = { some: 'header' };

      const url = new Url('http://localhost:3000');

      url.setDefaultHeaders(headers);
      expect(url.headers).to.deep.equal(headers);

      url.setDefaultHeaders();
      expect(url.headers).to.equal(null);
      done();
    });

    it('throws an error when setting headers that are not an object', (done) => {
      const url = new Url('http://localhost:3000');

      try {
        url.setDefaultHeaders('headers');
        expect.fail();
      } catch (e) {
        expect(e.message).to.equal(errorMessages.headers.notObject);
        done();
      }
    });
  });
});
