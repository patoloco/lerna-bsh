const { expect } = require('chai');
const _ = require('lodash');
const sinon = require('sinon');

const { errorMessages, UrlListByStage } = require('../../src');

const http = require('../../src/http');

const options = {
  prefixes: {
    DEV: 'http://localhost:3000',
    TEST: 'http://localhost:3001',
  },
  defaultPrefix: 'DEV',
};

describe('Unit: UrlListByStage', () => {
  describe('constructor', () => {
    it('throws an error if no options are provided', (done) => {
      try {
        const list = new UrlListByStage();
        expect.fail();
      } catch (e) {
        expect(e.message).to.equal(errorMessages.constructor.noOptions);
        done();
      }
    });

    it('throws an error if prefixes are not provided as an object', (done) => {
      try {
        const list = new UrlListByStage('test');
        expect.fail();
      } catch (e) {
        expect(e.message).to.equal(errorMessages.constructor.notObject);
        done();
      }
    });

    it('throws an error if no prefixes are provided', (done) => {
      const badOptions = _.cloneDeep(options);
      delete badOptions.prefixes.DEV;
      delete badOptions.prefixes.TEST;
      try {
        const list = new UrlListByStage(badOptions);
        expect.fail();
      } catch (e) {
        expect(e.message).to.equal(errorMessages.constructor.noPrefixes);
        done();
      }
    });

    it('throws an error if no default prefix is provided', (done) => {
      const badOptions = _.cloneDeep(options);
      delete badOptions.defaultPrefix;
      try {
        const list = new UrlListByStage(badOptions);
        expect.fail();
      } catch (e) {
        expect(e.message).to.equal(errorMessages.constructor.noDefault);
        done();
      }
    });

    it('throws an error if default prefix is not in prefix list', (done) => {
      const badOptions = _.cloneDeep(options);
      badOptions.defaultPrefix = 'xyz';
      try {
        const list = new UrlListByStage(badOptions);
        expect.fail();
      } catch (e) {
        expect(e.message).to.equal(errorMessages.constructor.invalidDefault);
        done();
      }
    });

    it('stores provided prefixes', (done) => {
      const list = new UrlListByStage(options);

      const keys = Array.from(list.prefixes.keys());
      expect(keys.length).to.equal(2);
      expect(keys).to.include('DEV', 'TEST');
      done();
    });
  });

  describe('getPrefix', () => {
    it('returns the current prefix', (done) => {
      const list = new UrlListByStage(options);

      const currentPrefix = list.getPrefix();
      expect(currentPrefix).to.equal(options.defaultPrefix);
      done();
    });
  });

  describe('getPrefixes', () => {
    it('retrieves stored prefixes', (done) => {
      const list = new UrlListByStage(options);

      const prefixes = list.getPrefixes();
      const devPrefix = prefixes.get('DEV');
      expect(devPrefix).to.equal(options.prefixes.DEV);
      done();
    });
  });

  describe('setPrefix', () => {
    it('fails to set a prefix that is not stored', (done) => {
      const list = new UrlListByStage(options);

      try {
        list.setPrefix('PROD');
        expect.fail();
      } catch (e) {
        expect(e.message).to.equal(errorMessages.setPrefix.notFound);
        done();
      }
    });

    it('set a valid prefix', (done) => {
      const list = new UrlListByStage(options);

      expect(list.prefix).to.equal('DEV');
      list.setPrefix('TEST');
      expect(list.prefix).to.equal('TEST');
      done();
    });
  });

  describe('headers', () => {
    let sandbox;
    beforeEach(() => {
      sandbox = sinon.createSandbox();
    });
    afterEach(() => {
      sandbox.restore();
    });

    it('throws an error if headers are not provided as an object', (done) => {
      try {
        const list = new UrlListByStage(options);
        list.setDefaultHeaders('head');
        expect.fail();
      } catch (e) {
        expect(e.message).to.equal(errorMessages.headers.notObject);
        done();
      }
    });

    it('sets defaultHeaders', (done) => {
      const list = new UrlListByStage(options);
      const headers = { some: 'header' };
      list.setDefaultHeaders(headers);
      expect(list.headers).to.deep.equal(headers);
      done();
    });

    it('sends default headers', async () => {
      const requestStub = sandbox.stub(http, 'get').resolves({});
      const headers = { some: 'header' };

      const list = new UrlListByStage(options);
      list.setPrefix('DEV');
      list.setDefaultHeaders(headers);

      await list.get('/v1/users');

      sinon.assert.calledOnce(requestStub);

      const expectedUrl = options.prefixes.DEV + '/v1/users';
      sinon.assert.calledWith(requestStub, expectedUrl, { headers });
    });

    it('can unset defaultHeaders', (done) => {
      const list = new UrlListByStage(options);
      const headers = { some: 'header' };
      list.setDefaultHeaders(headers);
      expect(list.headers).to.deep.equal(headers);

      list.setDefaultHeaders();
      expect(list.headers).to.equal(null);
      done();
    });
  });

  describe('http methods', () => {
    describe('get', () => {
      let sandbox;
      beforeEach(() => {
        sandbox = sinon.createSandbox();
      });
      afterEach(() => {
        sandbox.restore();
      });

      it('throws an error if no path is specified', async () => {
        const list = new UrlListByStage(options);

        try {
          await list.get('');
          expect.fail();
        } catch (e) {
          expect(e.message).to.equal(errorMessages.http.noPath);
        }
      });

      it('makes a request if path is provided without options', async () => {
        const requestStub = sandbox.stub(http, 'get').resolves({});
        const list = new UrlListByStage(options);
        list.setPrefix('DEV');

        await list.get('/v1/users');

        sinon.assert.calledOnce(requestStub);

        const expectedUrl = options.prefixes.DEV + '/v1/users';
        sinon.assert.calledWith(requestStub, expectedUrl, { headers: {} });
      });

      it('can send custom headers', async () => {
        const requestStub = sandbox.stub(http, 'get').resolves({});
        const headers = { some: 'header' };

        const list = new UrlListByStage(options);
        list.setPrefix('DEV');

        await list.get('/v1/users', headers);

        sinon.assert.calledOnce(requestStub);

        const expectedUrl = options.prefixes.DEV + '/v1/users';
        sinon.assert.calledWith(requestStub, expectedUrl, { headers });
      });

      it('throws an error with bad headers', async () => {
        const list = new UrlListByStage(options);
        const badHeaders = 'headers';

        try {
          await list.get('/v1/users', badHeaders);
          expect.fail();
        } catch (e) {
          expect(e.message).to.equal(errorMessages.http.badHeaders);
        }
      });

      it('can send custom headers alongside default headers', async () => {
        const requestStub = sandbox.stub(http, 'get').resolves({});
        const defaultHeaders = { default: 'headers' };
        const headers = { some: 'header' };

        const list = new UrlListByStage(options);
        list.setDefaultHeaders(defaultHeaders);
        list.setPrefix('DEV');

        await list.get('/v1/users', headers);

        sinon.assert.calledOnce(requestStub);

        const expectedUrl = options.prefixes.DEV + '/v1/users';
        const expectedHeaders = {
          headers: {
            some: 'header',
            default: 'headers',
          },
        };
        sinon.assert.calledWith(requestStub, expectedUrl, expectedHeaders);
      });
    });

    describe('post', () => {
      let sandbox;
      beforeEach(() => {
        sandbox = sinon.createSandbox();
      });
      afterEach(() => {
        sandbox.restore();
      });

      it('throws an error if no path is specified', async () => {
        const list = new UrlListByStage(options);

        try {
          await list.post('');
          expect.fail();
        } catch (e) {
          expect(e.message).to.equal(errorMessages.http.noPath);
        }
      });

      it('makes a request if path is provided without options', async () => {
        const requestStub = sandbox.stub(http, 'post').resolves({});
        const list = new UrlListByStage(options);
        list.setPrefix('DEV');

        await list.post('/v1/users');

        sinon.assert.calledOnce(requestStub);

        const expectedUrl = options.prefixes.DEV + '/v1/users';
        sinon.assert.calledWith(requestStub, expectedUrl, { body: {}, headers: {} });
      });

      it('can send custom headers', async () => {
        const requestStub = sandbox.stub(http, 'post').resolves({});
        const headers = { some: 'header' };

        const list = new UrlListByStage(options);
        list.setPrefix('DEV');

        await list.post('/v1/users', {}, headers);

        sinon.assert.calledOnce(requestStub);

        const expectedUrl = options.prefixes.DEV + '/v1/users';
        sinon.assert.calledWith(requestStub, expectedUrl, { body: {}, headers });
      });

      it('throws an error with bad headers', async () => {
        const list = new UrlListByStage(options);
        const badHeaders = 'headers';

        try {
          await list.post('/v1/users', {}, badHeaders);
          expect.fail();
        } catch (e) {
          expect(e.message).to.equal(errorMessages.http.badHeaders);
        }
      });

      it('can send custom headers alongside default headers', async () => {
        const requestStub = sandbox.stub(http, 'post').resolves({});
        const defaultHeaders = { default: 'headers' };
        const headers = { some: 'header' };

        const list = new UrlListByStage(options);
        list.setDefaultHeaders(defaultHeaders);
        list.setPrefix('DEV');

        await list.post('/v1/users', {}, headers);

        sinon.assert.calledOnce(requestStub);

        const expectedUrl = options.prefixes.DEV + '/v1/users';
        const expectedHeaders = {
          some: 'header',
          default: 'headers',
        };
        sinon.assert.calledWith(requestStub, expectedUrl, { body: {}, headers: expectedHeaders });
      });

      it('can post a body', async () => {
        const requestStub = sandbox.stub(http, 'post').resolves({});
        const headers = { some: 'header' };
        const body = { test: 123 };

        const list = new UrlListByStage(options);
        list.setPrefix('DEV');
        list.setDefaultHeaders(headers);

        await list.post('/v1/users', body);

        sinon.assert.calledOnce(requestStub);

        const expectedUrl = options.prefixes.DEV + '/v1/users';
        sinon.assert.calledWith(requestStub, expectedUrl, { body, headers });
      });
    });

    describe('put', () => {
      let sandbox;
      beforeEach(() => {
        sandbox = sinon.createSandbox();
      });
      afterEach(() => {
        sandbox.restore();
      });

      it('throws an error if no path is specified', async () => {
        const list = new UrlListByStage(options);

        try {
          await list.put('');
          expect.fail();
        } catch (e) {
          expect(e.message).to.equal(errorMessages.http.noPath);
        }
      });

      it('can send custom headers', async () => {
        const requestStub = sandbox.stub(http, 'put').resolves({});
        const headers = { some: 'header' };

        const list = new UrlListByStage(options);
        list.setPrefix('DEV');

        await list.put('/v1/users', {}, headers);

        sinon.assert.calledOnce(requestStub);

        const expectedUrl = options.prefixes.DEV + '/v1/users';
        sinon.assert.calledWith(requestStub, expectedUrl, { body: {}, headers });
      });

      it('throws an error with bad headers', async () => {
        const list = new UrlListByStage(options);
        const badHeaders = 'headers';

        try {
          await list.put('/v1/users', {}, badHeaders);
          expect.fail();
        } catch (e) {
          expect(e.message).to.equal(errorMessages.http.badHeaders);
        }
      });

      it('can send custom headers alongside default headers', async () => {
        const requestStub = sandbox.stub(http, 'put').resolves({});
        const defaultHeaders = { default: 'headers' };
        const headers = { some: 'header' };

        const list = new UrlListByStage(options);
        list.setDefaultHeaders(defaultHeaders);
        list.setPrefix('DEV');

        await list.put('/v1/users', {}, headers);

        sinon.assert.calledOnce(requestStub);

        const expectedUrl = options.prefixes.DEV + '/v1/users';
        const expectedHeaders = {
          some: 'header',
          default: 'headers',
        };
        sinon.assert.calledWith(requestStub, expectedUrl, { body: {}, headers: expectedHeaders });
      });

      it('can put a body', async () => {
        const requestStub = sandbox.stub(http, 'put').resolves({});
        const headers = { some: 'header' };
        const body = { test: 123 };

        const list = new UrlListByStage(options);
        list.setPrefix('DEV');
        list.setDefaultHeaders(headers);

        await list.put('/v1/users', body);

        sinon.assert.calledOnce(requestStub);

        const expectedUrl = options.prefixes.DEV + '/v1/users';
        sinon.assert.calledWith(requestStub, expectedUrl, { body, headers });
      });
    });

    describe('delete', () => {
      let sandbox;
      beforeEach(() => {
        sandbox = sinon.createSandbox();
      });
      afterEach(() => {
        sandbox.restore();
      });

      it('throws an error if no path is specified', async () => {
        const list = new UrlListByStage(options);

        try {
          await list.delete('');
          expect.fail();
        } catch (e) {
          expect(e.message).to.equal(errorMessages.http.noPath);
        }
      });

      it('can send custom headers', async () => {
        const requestStub = sandbox.stub(http, 'delete').resolves({});
        const headers = { some: 'header' };

        const list = new UrlListByStage(options);
        list.setPrefix('DEV');

        await list.delete('/v1/users', headers);

        sinon.assert.calledOnce(requestStub);

        const expectedUrl = options.prefixes.DEV + '/v1/users';
        sinon.assert.calledWith(requestStub, expectedUrl, { headers });
      });

      it('throws an error with bad headers', async () => {
        const list = new UrlListByStage(options);
        const badHeaders = 'headers';

        try {
          await list.delete('/v1/users', badHeaders);
          expect.fail();
        } catch (e) {
          expect(e.message).to.equal(errorMessages.http.badHeaders);
        }
      });

      it('can send custom headers alongside default headers', async () => {
        const requestStub = sandbox.stub(http, 'delete').resolves({});
        const defaultHeaders = { default: 'headers' };
        const headers = { some: 'header' };

        const list = new UrlListByStage(options);
        list.setDefaultHeaders(defaultHeaders);
        list.setPrefix('DEV');

        await list.delete('/v1/users', headers);

        sinon.assert.calledOnce(requestStub);

        const expectedUrl = options.prefixes.DEV + '/v1/users';
        const expectedHeaders = {
          headers: {
            some: 'header',
            default: 'headers',
          },
        };
        sinon.assert.calledWith(requestStub, expectedUrl, expectedHeaders);
      });
    });
  });
});
