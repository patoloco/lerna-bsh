const { expect } = require('chai');
const sinon = require('sinon');

const { errorMessages, UrlList } = require('../../src');

const http = require('../../src/http');

const sampleUrls = {
  animals: 'http://localhost:3000',
  plants: 'http://localhost:3001',
};

describe('Unit: UrlList', () => {
  describe('constructor', () => {
    it('throws an error if no options are provided', (done) => {
      try {
        const list = new UrlList();
        expect.fail();
      } catch (e) {
        expect(e.message).to.equal(errorMessages.constructor.noOptions);
        done();
      }
    });

    it('throws an error if options are not provided as an object', (done) => {
      try {
        const list = new UrlList('test');
        expect.fail();
      } catch (e) {
        expect(e.message).to.equal(errorMessages.constructor.notObject);
        done();
      }
    });

    it('throws an error if no urls are provided', (done) => {
      try {
        const list = new UrlList({});
        expect.fail();
      } catch (e) {
        expect(e.message).to.equal(errorMessages.constructor.noUrls);
        done();
      }
    });

    it('stores provided urls', (done) => {
      const list = new UrlList(sampleUrls);

      const keys = Object.keys(list.urls);
      expect(keys.length).to.equal(2);
      expect(keys).to.include('animals', 'plants');
      done();
    });
  });

  describe('getUrls', () => {
    it('retrieves stored prefixes', (done) => {
      const list = new UrlList(sampleUrls);

      const urls = list.getUrls();
      const keys = Object.keys(urls);
      expect(keys).to.include('animals', 'plants');
      done();
    });
  });

  describe('setDefaultHeaders', () => {
    let sandbox;
    beforeEach(() => {
      sandbox = sinon.createSandbox();
    });
    afterEach(() => {
      sandbox.restore();
    });

    it('throws an error if headers are not provided as an object', (done) => {
      try {
        const list = new UrlList(sampleUrls);
        list.setDefaultHeaders('head');
        expect.fail();
      } catch (e) {
        expect(e.message).to.equal(errorMessages.headers.notObject);
        done();
      }
    });

    it('sets defaultHeaders', (done) => {
      const list = new UrlList(sampleUrls);
      const headers = { some: 'header' };
      list.setDefaultHeaders(headers);
      expect(list.urls.animals.headers).to.deep.equal(headers);
      expect(list.urls.plants.headers).to.deep.equal(headers);
      done();
    });

    it('uses defaultHeaders in a request', async () => {
      const requestStub = sandbox.stub(http, 'get').resolves({});
      const headers = { some: 'header' };

      const list = new UrlList(sampleUrls);
      list.setDefaultHeaders(headers);

      await list.urls.animals.get('/v1/users');

      sinon.assert.calledOnce(requestStub);

      const expectedUrl = sampleUrls.animals + '/v1/users';
      sinon.assert.calledWith(requestStub, expectedUrl, { headers });
    });

    it('can unset defaultHeaders', (done) => {
      const list = new UrlList(sampleUrls);
      const headers = { some: 'header' };
      list.setDefaultHeaders(headers);
      expect(list.urls.animals.headers).to.deep.equal(headers);
      expect(list.urls.plants.headers).to.deep.equal(headers);

      list.setDefaultHeaders();
      expect(list.headers).to.equal(null);
      done();
    });
  });

  describe('http methods', () => {
    describe('get', () => {
      let sandbox;
      beforeEach(() => {
        sandbox = sinon.createSandbox();
      });
      afterEach(() => {
        sandbox.restore();
      });

      it('throws an error if no path is specified', async () => {
        const list = new UrlList(sampleUrls);

        try {
          await list.urls.animals.get('');
          expect.fail();
        } catch (e) {
          expect(e.message).to.equal(errorMessages.http.noPath);
        }
      });

      it('can set headers for the request', async () => {
        const requestStub = sandbox.stub(http, 'get').resolves({});
        const headers = { some: 'header' };

        const list = new UrlList(sampleUrls);

        await list.urls.animals.get('/v1/users', headers);

        sinon.assert.calledOnce(requestStub);

        const expectedUrl = sampleUrls.animals + '/v1/users';
        sinon.assert.calledWith(requestStub, expectedUrl, { headers });
      });

      it('throws an error with bad headers', async () => {
        const list = new UrlList(sampleUrls);
        const badHeaders = 'headers';

        try {
          await list.urls.animals.get('/v1/users', badHeaders);
          expect.fail();
        } catch (e) {
          expect(e.message).to.equal(errorMessages.http.badHeaders);
        }
      });

      it('can set headers for the request alongside default headers', async () => {
        const requestStub = sandbox.stub(http, 'get').resolves({});
        const defaultHeaders = { default: 'headers' };
        const headers = { some: 'header' };

        const list = new UrlList(sampleUrls);
        list.setDefaultHeaders(defaultHeaders);

        await list.urls.animals.get('/v1/users', headers);

        sinon.assert.calledOnce(requestStub);

        const expectedUrl = sampleUrls.animals + '/v1/users';
        const expectedHeaders = {
          headers: {
            some: 'header',
            default: 'headers',
          },
        };
        sinon.assert.calledWith(requestStub, expectedUrl, expectedHeaders);
      });
    });

    describe('post', () => {
      let sandbox;
      beforeEach(() => {
        sandbox = sinon.createSandbox();
      });
      afterEach(() => {
        sandbox.restore();
      });

      it('throws an error if no path is specified', async () => {
        const list = new UrlList(sampleUrls);

        try {
          await list.urls.animals.post('');
          expect.fail();
        } catch (e) {
          expect(e.message).to.equal(errorMessages.http.noPath);
        }
      });

      it('makes a request if path is provided without options', async () => {
        const requestStub = sandbox.stub(http, 'post').resolves({});
        const list = new UrlList(sampleUrls);

        await list.urls.animals.post('/v1/users');

        sinon.assert.calledOnce(requestStub);

        const expectedUrl = sampleUrls.animals + '/v1/users';
        sinon.assert.calledWith(requestStub, expectedUrl, { body: {}, headers: {} });
      });

      it('can set headers for the request', async () => {
        const requestStub = sandbox.stub(http, 'post').resolves({});
        const headers = { some: 'header' };

        const list = new UrlList(sampleUrls);

        await list.urls.animals.post('/v1/users', {}, headers);

        sinon.assert.calledOnce(requestStub);

        const expectedUrl = sampleUrls.animals + '/v1/users';
        sinon.assert.calledWith(requestStub, expectedUrl, { body: {}, headers });
      });

      it('throws an error with bad headers', async () => {
        const list = new UrlList(sampleUrls);
        const badHeaders = 'headers';

        try {
          await list.urls.animals.post('/v1/users', {}, badHeaders);
          expect.fail();
        } catch (e) {
          expect(e.message).to.equal(errorMessages.http.badHeaders);
        }
      });

      it('can set headers for the request alongside default headers', async () => {
        const requestStub = sandbox.stub(http, 'post').resolves({});
        const defaultHeaders = { default: 'headers' };
        const headers = { some: 'header' };

        const list = new UrlList(sampleUrls);
        list.setDefaultHeaders(defaultHeaders);

        await list.urls.animals.post('/v1/users', {}, headers);

        sinon.assert.calledOnce(requestStub);

        const expectedUrl = sampleUrls.animals + '/v1/users';
        const expectedHeaders = {
          some: 'header',
          default: 'headers',
        };
        sinon.assert.calledWith(requestStub, expectedUrl, { body: {}, headers: expectedHeaders });
      });

      it('can post a body', async () => {
        const requestStub = sandbox.stub(http, 'post').resolves({});
        const headers = { some: 'header' };
        const body = { test: 123 };

        const list = new UrlList(sampleUrls);
        list.setDefaultHeaders(headers);

        await list.urls.animals.post('/v1/users', body);

        sinon.assert.calledOnce(requestStub);

        const expectedUrl = sampleUrls.animals + '/v1/users';
        sinon.assert.calledWith(requestStub, expectedUrl, { body, headers });
      });
    });

    describe('put', () => {
      let sandbox;
      beforeEach(() => {
        sandbox = sinon.createSandbox();
      });
      afterEach(() => {
        sandbox.restore();
      });

      it('throws an error if no path is specified', async () => {
        const list = new UrlList(sampleUrls);

        try {
          await list.urls.animals.put('');
          expect.fail();
        } catch (e) {
          expect(e.message).to.equal(errorMessages.http.noPath);
        }
      });

      it('makes a request if path is provided without options', async () => {
        const requestStub = sandbox.stub(http, 'put').resolves({});
        const list = new UrlList(sampleUrls);

        await list.urls.animals.put('/v1/users');

        sinon.assert.calledOnce(requestStub);

        const expectedUrl = sampleUrls.animals + '/v1/users';
        sinon.assert.calledWith(requestStub, expectedUrl, { body: {}, headers: {} });
      });

      it('can set headers for the request', async () => {
        const requestStub = sandbox.stub(http, 'put').resolves({});
        const headers = { some: 'headers' };

        const list = new UrlList(sampleUrls);

        await list.urls.animals.put('/v1/users', {}, headers);

        sinon.assert.calledOnce(requestStub);

        const expectedUrl = sampleUrls.animals + '/v1/users';
        sinon.assert.calledWith(requestStub, expectedUrl, { body: {}, headers });
      });

      it('throws an error with bad headers', async () => {
        const list = new UrlList(sampleUrls);
        const badHeaders = 'headers';

        try {
          await list.urls.animals.put('/v1/users', {}, badHeaders);
          expect.fail();
        } catch (e) {
          expect(e.message).to.equal(errorMessages.http.badHeaders);
        }
      });

      it('can set headers for the request alongside default headers', async () => {
        const requestStub = sandbox.stub(http, 'put').resolves({});
        const defaultHeaders = { default: 'headers' };
        const headers = { some: 'header' };

        const list = new UrlList(sampleUrls);
        list.setDefaultHeaders(defaultHeaders);

        await list.urls.animals.put('/v1/users', {}, headers);

        sinon.assert.calledOnce(requestStub);

        const expectedUrl = sampleUrls.animals + '/v1/users';
        const expectedHeaders = {
          some: 'header',
          default: 'headers',
        };
        sinon.assert.calledWith(requestStub, expectedUrl, { body: {}, headers: expectedHeaders });
      });

      it('can put a body', async () => {
        const requestStub = sandbox.stub(http, 'put').resolves({});
        const headers = { some: 'header' };
        const body = { test: 123 };

        const list = new UrlList(sampleUrls);
        list.setDefaultHeaders(headers);

        await list.urls.animals.put('/v1/users', body);

        sinon.assert.calledOnce(requestStub);

        const expectedUrl = sampleUrls.animals + '/v1/users';
        sinon.assert.calledWith(requestStub, expectedUrl, { body, headers });
      });
    });

    describe('delete', () => {
      let sandbox;
      beforeEach(() => {
        sandbox = sinon.createSandbox();
      });
      afterEach(() => {
        sandbox.restore();
      });

      it('throws an error if no path is specified', async () => {
        const list = new UrlList(sampleUrls);

        try {
          await list.urls.animals.delete('');
          expect.fail();
        } catch (e) {
          expect(e.message).to.equal(errorMessages.http.noPath);
        }
      });

      it('can set headers for the request', async () => {
        const requestStub = sandbox.stub(http, 'delete').resolves({});
        const headers = { some: 'header' };

        const list = new UrlList(sampleUrls);

        await list.urls.animals.delete('/v1/users', headers);

        sinon.assert.calledOnce(requestStub);

        const expectedUrl = sampleUrls.animals + '/v1/users';
        sinon.assert.calledWith(requestStub, expectedUrl, { headers });
      });

      it('throws an error with bad headers', async () => {
        const list = new UrlList(sampleUrls);
        const badHeaders = 'headers';

        try {
          await list.urls.animals.delete('/v1/users', badHeaders);
          expect.fail();
        } catch (e) {
          expect(e.message).to.equal(errorMessages.http.badHeaders);
        }
      });

      it('can set headers for the request alongside the default headers', async () => {
        const requestStub = sandbox.stub(http, 'delete').resolves({});
        const defaultHeaders = { default: 'headers' };
        const headers = { some: 'header' };

        const list = new UrlList(sampleUrls);
        list.setDefaultHeaders(defaultHeaders);

        await list.urls.animals.delete('/v1/users', headers);

        sinon.assert.calledOnce(requestStub);

        const expectedUrl = sampleUrls.animals + '/v1/users';
        const expectedHeaders = {
          headers: {
            some: 'header',
            default: 'headers',
          },
        };
        sinon.assert.calledWith(requestStub, expectedUrl, expectedHeaders);
      });
    });
  });
});
