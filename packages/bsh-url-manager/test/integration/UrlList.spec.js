const { expect } = require('chai');

const { app } = require('../server');

const { UrlList } = require('../../src');

const sampleUrls = {
  animals: 'http://localhost:3000',
};

describe('Integration: UrlList tests with Express server', () => {
  describe('GET', () => {
    let list;
    beforeEach(() => {
      list = new UrlList(sampleUrls);
    });

    it('gets from a live server', async () => {
      const response = await list.urls.animals.get('/');
      expect(response).to.have.property('body');
      const { reqBody } = response.body;
      expect(reqBody).to.deep.equal({});
    });

    it('can send default headers', async () => {
      const headers = { some: 'header' };
      list.setDefaultHeaders(headers);
      const response = await list.urls.animals.get('/');
      expect(response).to.have.property('body');

      const { reqBody, reqHeaders } = response.body;
      expect(reqBody).to.deep.equal({});
      expect(reqHeaders).to.include(headers);
    });

    it('can send custom headers', async () => {
      const headers = { some: 'header' };
      const response = await list.urls.animals.get('/', headers);
      expect(response).to.have.property('body');

      const { reqBody, reqHeaders } = response.body;
      expect(reqBody).to.deep.equal({});
      expect(reqHeaders).to.include(headers);
    });

    it('handles a 404 response', async () => {
      const response = await list.urls.animals.get('/xyz/abc/123');
      expect(response.statusCode).to.equal(404);
    });
  });

  describe('POST', () => {
    let list;
    beforeEach(() => {
      list = new UrlList(sampleUrls);
    });

    it('posts to a live server with no body', async () => {
      const response = await list.urls.animals.post('/');
      expect(response).to.have.property('body');

      const { reqBody } = response.body;
      expect(reqBody).to.deep.equal({});
    });

    it('posts to a live server with a body', async () => {
      const body = { some: 'body' };

      const response = await list.urls.animals.post('/', body);
      expect(response).to.have.property('body');

      const { reqBody } = response.body;
      expect(reqBody).to.deep.equal(body);
    });

    it('posts to a live server with custom headers', async () => {
      const headers = { these: 'headers' };
      list.setDefaultHeaders(headers);
      const response = await list.urls.animals.post('/');
      expect(response).to.have.property('body');

      const { reqBody, reqHeaders } = response.body;
      expect(reqBody).to.deep.equal({});
      expect(reqHeaders).to.include(headers);
    });

    it('handles a 404 response', async () => {
      const response = await list.urls.animals.post('/xyz/abc/123');
      expect(response.statusCode).to.equal(404);
    });
  });

  describe('PUT', () => {
    let list;
    beforeEach(() => {
      list = new UrlList(sampleUrls);
    });

    it('posts to a live server with no body', async () => {
      const response = await list.urls.animals.put('/');
      expect(response).to.have.property('body');

      const { reqBody } = response.body;
      expect(reqBody).to.deep.equal({});
    });

    it('posts to a live server with a body', async () => {
      const body = { some: 'body' };

      const response = await list.urls.animals.put('/', body);
      expect(response).to.have.property('body');

      const { reqBody } = response.body;
      expect(reqBody).to.deep.equal(body);
    });

    it('posts to a live server with custom headers', async () => {
      const headers = { these: 'headers' };
      list.setDefaultHeaders(headers);
      const response = await list.urls.animals.put('/');
      expect(response).to.have.property('body');

      const { reqBody, reqHeaders } = response.body;
      expect(reqBody).to.deep.equal({});
      expect(reqHeaders).to.include(headers);
    });

    it('handles a 404 response', async () => {
      const response = await list.urls.animals.put('/xyz/abc/123');
      expect(response.statusCode).to.equal(404);
    });
  });

  describe('DELETE', () => {
    let list;
    beforeEach(() => {
      list = new UrlList(sampleUrls);
    });

    it('deletes from a live server', async () => {
      const response = await list.urls.animals.delete('/');
      expect(response).to.have.property('body');
      const { reqBody } = response.body;
      expect(reqBody).to.deep.equal({});
    });

    it('can send custom headers', async () => {
      const headers = { some: 'header' };
      list.setDefaultHeaders(headers);
      const response = await list.urls.animals.delete('/');
      expect(response).to.have.property('body');

      const { reqBody, reqHeaders } = response.body;
      expect(reqBody).to.deep.equal({});
      expect(reqHeaders).to.include(headers);
    });

    it('handles a 404 response', async () => {
      const response = await list.urls.animals.delete('/xyz/abc/123');
      expect(response.statusCode).to.equal(404);
    });
  });
});
