const { expect } = require('chai');
const request = require('request');
const sinon = require('sinon');

const { app, PORT } = require('../server');

const { UrlListByStage } = require('../../src');

const options = {
  prefixes: {
    DEV: `http://localhost:${PORT}`,
  },
  defaultPrefix: 'DEV',
};

describe('Integration: UrlListByStage tests with Express server', () => {
  describe('GET', () => {
    let list;
    let sandbox;
    beforeEach(() => {
      list = new UrlListByStage(options);
      sandbox = sinon.createSandbox();
    });
    afterEach(() => {
      sandbox.restore();
    });

    it('gets from a live server', async () => {
      const response = await list.get('/');
      expect(response).to.have.property('body');
      const { reqBody } = response.body;
      expect(reqBody).to.deep.equal({});
    });

    it('can send custom headers', async () => {
      const headers = { some: 'header' };
      list.setDefaultHeaders(headers);
      const response = await list.get('/');
      expect(response).to.have.property('body');

      const { reqBody, reqHeaders } = response.body;
      expect(reqBody).to.deep.equal({});
      expect(reqHeaders).to.include(headers);
    });

    it('handles a 404 response', async () => {
      const response = await list.get('/xyz/abc/123');
      expect(response.statusCode).to.equal(404);
    });

    it('handles http errors', async () => {
      const stub = sandbox.stub(request, 'get')
        .callsFake((url, cb) => cb(new Error('error'), null, null));

      try {
        const response = await list.get('/');
        expect.fail();
      } catch (e) {
        expect(e.message).to.equal('error');
        sinon.assert.calledOnce(stub);
      }
    });
  });

  describe('POST', () => {
    let list;
    let sandbox;
    beforeEach(() => {
      list = new UrlListByStage(options);
      sandbox = sinon.createSandbox();
    });
    afterEach(() => {
      sandbox.restore();
    });

    it('posts to a live server with no body', async () => {
      const response = await list.post('/');
      expect(response).to.have.property('body');

      const { reqBody } = response.body;
      expect(reqBody).to.deep.equal({});
    });

    it('posts to a live server with a body', async () => {
      const body = { some: 'body' };

      const response = await list.post('/', body);
      expect(response).to.have.property('body');

      const { reqBody } = response.body;
      expect(reqBody).to.deep.equal(body);
    });

    it('posts to a live server with custom headers', async () => {
      const headers = { these: 'headers' };
      list.setDefaultHeaders(headers);
      const response = await list.post('/');
      expect(response).to.have.property('body');

      const { reqBody, reqHeaders } = response.body;
      expect(reqBody).to.deep.equal({});
      expect(reqHeaders).to.include(headers);
    });

    it('handles a 404 response', async () => {
      const response = await list.post('/xyz/abc/123');
      expect(response.statusCode).to.equal(404);
    });

    it('handles http errors', async () => {
      const stub = sandbox.stub(request, 'post')
        .callsFake((url, cb) => cb(new Error('error'), null, null));

      try {
        const response = await list.post('/');
        expect.fail();
      } catch (e) {
        expect(e.message).to.equal('error');
        sinon.assert.calledOnce(stub);
      }
    });
  });

  describe('PUT', () => {
    let list;
    let sandbox;
    beforeEach(() => {
      list = new UrlListByStage(options);
      sandbox = sinon.createSandbox();
    });
    afterEach(() => {
      sandbox.restore();
    });

    it('posts to a live server with no body', async () => {
      const response = await list.put('/');
      expect(response).to.have.property('body');

      const { reqBody } = response.body;
      expect(reqBody).to.deep.equal({});
    });

    it('posts to a live server with a body', async () => {
      const body = { some: 'body' };

      const response = await list.put('/', body);
      expect(response).to.have.property('body');

      const { reqBody } = response.body;
      expect(reqBody).to.deep.equal(body);
    });

    it('posts to a live server with custom headers', async () => {
      const headers = { these: 'headers' };
      list.setDefaultHeaders(headers);
      const response = await list.put('/');
      expect(response).to.have.property('body');

      const { reqBody, reqHeaders } = response.body;
      expect(reqBody).to.deep.equal({});
      expect(reqHeaders).to.include(headers);
    });

    it('handles a 404 response', async () => {
      const response = await list.put('/xyz/abc/123');
      expect(response.statusCode).to.equal(404);
    });

    it('handles http errors', async () => {
      const stub = sandbox.stub(request, 'put')
        .callsFake((url, cb) => cb(new Error('error'), null, null));

      try {
        const response = await list.put('/');
        expect.fail();
      } catch (e) {
        expect(e.message).to.equal('error');
        sinon.assert.calledOnce(stub);
      }
    });
  });

  describe('DELETE', () => {
    let list;
    let sandbox;
    beforeEach(() => {
      list = new UrlListByStage(options);
      sandbox = sinon.createSandbox();
    });
    afterEach(() => {
      sandbox.restore();
    });

    it('deletes from a live server', async () => {
      const response = await list.delete('/');
      expect(response).to.have.property('body');
      const { reqBody } = response.body;
      expect(reqBody).to.deep.equal({});
    });

    it('can send custom headers', async () => {
      const headers = { some: 'header' };
      list.setDefaultHeaders(headers);
      const response = await list.delete('/');
      expect(response).to.have.property('body');

      const { reqBody, reqHeaders } = response.body;
      expect(reqBody).to.deep.equal({});
      expect(reqHeaders).to.include(headers);
    });

    it('handles a 404 response', async () => {
      const response = await list.delete('/xyz/abc/123');
      expect(response.statusCode).to.equal(404);
    });

    it('handles http errors', async () => {
      const stub = sandbox.stub(request, 'delete')
        .callsFake((url, cb) => cb(new Error('error'), null, null));

      try {
        const response = await list.delete('/');
        expect.fail();
      } catch (e) {
        expect(e.message).to.equal('error');
        sinon.assert.calledOnce(stub);
      }
    });
  });
});
