const bodyParser = require('body-parser');
const express = require('express');

const app = express();
app.use(bodyParser.json());

const PORT = 3000;

app.post('/', (req, res) => {
  const response = {
    reqBody: req.body,
    reqHeaders: req.headers,
  };
  res.status(200).json(response);
});

app.get('/', (req, res) => {
  const response = {
    reqBody: req.body,
    reqHeaders: req.headers,
  };
  res.status(200).json(response);
});

app.put('/', (req, res) => {
  const response = {
    reqBody: req.body,
    reqHeaders: req.headers,
  };
  res.status(200).json(response);
});

app.delete('/', (req, res) => {
  const response = {
    reqBody: req.body,
    reqHeaders: req.headers,
  };
  res.status(200).json(response);
});

app.listen(PORT);

module.exports = {
  app,
  PORT,
};
