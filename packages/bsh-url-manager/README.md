```
const { UrlList, UrlListByStage} = require('bsh-url-manager');
```

```
const stages = {
  prefixes: {
    LOCAL: 'http://localhost:3000',
    DEV: 'dev.dev.dev',
    TEST: 'test.test.test',
  },
  defaultPrefix: 'LOCAL',
}
const list = new UrlListByStage(stages);

async function test() {
  console.log(list.getPrefixes()); // Map: LOCAL, DEV, TEST
  console.log(list.getPrefix()); // LOCAL
  try {
    const response = await list.get('/');
    console.log(response.body);
  } catch (e) {
    console.error(e);
  }

  try {
    list.setPrefix('DEV');
    console.log(list.getPrefix()); // DEV
  } catch (e) {
    console.error(e);
  }
}
```

```
const services = {
  serviceOne: 'http://localhost:3000',
  serviceTwo: 'http://localhost:3001',
};
const listTwo = new UrlList(services);

async function testTwo() {
  console.log(listTwo.getUrls()); // localhost:3000, localhost:3001
  listTwo.setDefaultHeaders({ some: 'header' });
  console.log(listTwo.urls.serviceOne.headers); // { some: 'header' }

  try {
    const response = await listTwo.urls.serviceOne.get('/');
    console.log(response.body);
  } catch (e) {
    console.error(e);
  }
}
```

```
(async function main() {
  await test();
  await testTwo();
})();
```
