```
const { LogLevel, Logman } = require('bsh-logman');

const bodyParser = require('body-parser');
const express = require('express');
const request = require('request');
```

```
function externalCall(data) {
  const options = {
    url: 'http://localhost:3000/log',
    json: { data },
    method: 'POST',
  };
  request(options);
}
```

```
const log = new Logman('core');
log.setLevel(LogLevel.DEBUG);
log.setExternalLevel(LogLevel.DEBUG);
log.setCallToExternal(externalCall);

const app = express();
app.use(bodyParser.json());

app.get('/', (req, res) => {
  log.error('bad, bad error');
  log.info('ultra useful info');
  log.debug('basic debug stuff');
  res.status(200).json({});
});

app.post('/log', (req, res) => {
  console.log(req.body.data);
  res.status(200).json({});
});

app.listen(3000);
```
