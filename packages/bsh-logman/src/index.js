/* eslint-disable no-console */
/* eslint-disable no-restricted-syntax */
const LogLevel = {
  NONE: 'NONE',
  ERROR: 'ERROR',
  INFO: 'INFO',
  DEBUG: 'DEBUG',
};

class Logman {
  constructor(name = 'default') {
    if (typeof name !== 'string') {
      throw new Error('name must be a string');
    }

    this.logLevels = {
      NONE: 1,
      ERROR: 2,
      INFO: 3,
      DEBUG: 4,
    };

    this.name = name;
    this.setLevel(LogLevel.DEBUG);
    this.setExternalLevel(LogLevel.NONE);

    this.setCallToExternal((x) => {}); // eslint-disable-line
  }

  setLevel(level) {
    if (this.logLevels[level]) {
      this.level = this.logLevels[level];
    } else {
      throw new Error('invalid log level');
    }
  }

  setExternalLevel(level) {
    if (this.logLevels[level]) {
      this.externalLevel = this.logLevels[level];
    } else {
      throw new Error('invalid log level');
    }
  }

  setCallToExternal(f) {
    if (typeof f !== 'function') {
      throw new Error('setCallToExternal must be passed a function');
    } else if (f.length !== 1) {
      throw new Error('setCallToExternal requires a function with a single argument');
    }
    this.callToExternal = f;
  }

  error(...args) {
    if (this.level >= this.logLevels[LogLevel.ERROR]) {
      const now = new Date().toUTCString();

      for (const arg of args) {
        let logString = `${this.name} (error) - ${now}: `;
        if (typeof arg === 'object') {
          logString += JSON.stringify(arg);
        } else {
          logString += arg.toString();
        }
        console.error(`${logString}`);

        if (this.externalLevel >= this.logLevels[LogLevel.ERROR]) {
          this.callToExternal(logString);
        }
      }
    }
  }

  info(...args) {
    if (this.level >= this.logLevels[LogLevel.INFO]) {
      const now = new Date().toUTCString();

      for (const arg of args) {
        let logString = `${this.name} (info) - ${now}: `;
        if (typeof arg === 'object') {
          logString += JSON.stringify(arg);
        } else {
          logString += arg.toString();
        }
        console.info(`${logString}`);

        if (this.externalLevel >= this.logLevels[LogLevel.INFO]) {
          this.callToExternal(logString);
        }
      }
    }
  }

  debug(...args) {
    if (this.level >= this.logLevels[LogLevel.DEBUG]) {
      const now = new Date().toUTCString();

      for (const arg of args) {
        let logString = `${this.name} (debug) - ${now}: `;
        if (typeof arg === 'object') {
          logString += JSON.stringify(arg);
        } else {
          logString += arg.toString();
        }
        console.debug(`${logString}`);

        if (this.externalLevel >= this.logLevels[LogLevel.DEBUG]) {
          this.callToExternal(logString);
        }
      }
    }
  }
}

module.exports = {
  LogLevel,
  Logman,
};
