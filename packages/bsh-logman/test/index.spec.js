const { expect } = require('chai');
const sinon = require('sinon');

const { LogLevel, Logman } = require('../src');

describe('Logman', () => {
  describe('constructor', () => {
    it('throws an error if name is not a string', (done) => {
      try {
        const log = new Logman(3);
        expect.fail();
      } catch (e) {
        expect(e.message).to.equal('name must be a string');
        done();
      }
    });

    it('can set the logger name', (done) => {
      const log = new Logman('core');
      expect(log.name).to.equal('core');
      done();
    });
  });

  describe('setLevel', () => {
    it('throws an error if specified level is invalid', (done) => {
      try {
        const log = new Logman();
        log.setLevel('asdfasdf');
        expect.fail();
      } catch (e) {
        expect(e.message).to.equal('invalid log level');
        done();
      }
    });

    it('can set logger to a valid level', (done) => {
      const log = new Logman();
      log.setLevel(LogLevel.INFO);
      done();
    });
  });

  describe('setExternalLevel', () => {
    it('throws an error if specified level is invalid', (done) => {
      try {
        const log = new Logman();
        log.setExternalLevel('asdfasdf');
        expect.fail();
      } catch (e) {
        expect(e.message).to.equal('invalid log level');
        done();
      }
    });

    it('can set external logger to a valid level', (done) => {
      const log = new Logman();
      log.setExternalLevel(LogLevel.INFO);
      done();
    });
  });

  describe('setCallToExternal', () => {
    let log;
    let sandbox;
    beforeEach(() => {
      log = new Logman();
      sandbox = sinon.createSandbox();
    });
    afterEach(() => {
      sandbox.restore();
    });

    it('throws an error if argument provided is not a function', (done) => {
      try {
        log.setCallToExternal('not a function');
        expect.fail();
      } catch (e) {
        expect(e.message).to.equal('setCallToExternal must be passed a function');
        done();
      }
    });

    it('has callToExternal set to a dummy function by default', (done) => {
      log.setLevel(LogLevel.DEBUG);
      log.setExternalLevel(LogLevel.DEBUG);

      // does not need log.setCallToExternal(f) to be set
      log.debug('test');
      done();
    });

    it('throws an error if function provided does not have a single argument', (done) => {
      function badFunction() {}
      try {
        log.setCallToExternal(badFunction);
        expect.fail();
      } catch (e) {
        expect(e.message).to.equal('setCallToExternal requires a function with a single argument');
        done();
      }
    });

    it('can set external call function to a function with a single argument', (done) => {
      const dummyFunction = (data) => data;
      const dummyFunctionString = dummyFunction.toString();

      log.setCallToExternal(dummyFunction);
      expect(log.callToExternal.toString()).to.equal(dummyFunctionString);
      done();
    });
  });

  describe('log calls', () => {
    describe('debug', () => {
      let consoleDebugStub;
      let log;
      let sandbox;
      beforeEach(() => {
        sandbox = sinon.createSandbox();
        log = new Logman();
        log.setLevel(LogLevel.DEBUG);
        log.setExternalLevel(LogLevel.NONE);
        consoleDebugStub = sandbox.stub(console, 'debug');
      });
      afterEach(() => {
        sandbox.restore();
      });

      it('logs a single line with a single log.debug call', (done) => {
        log.debug('test');
        sinon.assert.calledOnce(consoleDebugStub);
        done();
      });

      it('logs 3 line with a 3 argument log.debug call', (done) => {
        log.debug('test1', 'test2', 'test3');
        sinon.assert.callCount(consoleDebugStub, 3);
        done();
      });

      it('logs an object as a string', (done) => {
        const error = { statusCode: 500, message: 'internal server error' };
        log.debug(error);
        sinon.assert.calledOnce(consoleDebugStub);
        done();
      });

      it('does not log when level is set to ERROR', (done) => {
        log.setLevel(LogLevel.ERROR);
        log.debug('testtest');
        sinon.assert.callCount(consoleDebugStub, 0);
        done();
      });

      it('does not log when level is set to INFO', (done) => {
        log.setLevel(LogLevel.INFO);
        log.debug('testtest');
        sinon.assert.callCount(consoleDebugStub, 0);
        done();
      });

      it('makes external call when level is set to DEBUG', (done) => {
        log.setLevel(LogLevel.DEBUG);
        log.setExternalLevel(LogLevel.DEBUG);

        const externalStub = sandbox.stub(log, 'callToExternal');
        log.debug('test');
        sinon.assert.calledOnce(externalStub);

        done();
      });

      it('does not make external call when externalLevel is set to NONE', (done) => {
        log.setLevel(LogLevel.DEBUG);
        log.setExternalLevel(LogLevel.NONE);

        const externalStub = sandbox.stub(log, 'callToExternal');
        log.debug('test');
        sinon.assert.callCount(externalStub, 0);

        done();
      });
    });

    describe('error', () => {
      let consoleErrorStub;
      let log;
      let sandbox;
      beforeEach(() => {
        sandbox = sinon.createSandbox();
        log = new Logman();
        log.setLevel(LogLevel.ERROR);
        log.setExternalLevel(LogLevel.NONE);
        consoleErrorStub = sandbox.stub(console, 'error');
      });
      afterEach(() => {
        sandbox.restore();
      });

      it('logs a single line with a single log.error call', (done) => {
        log.error('test');
        sinon.assert.calledOnce(consoleErrorStub);
        done();
      });

      it('logs 3 line with a 3 argument log.error call', (done) => {
        log.error('test1', 'test2', 'test3');
        sinon.assert.callCount(consoleErrorStub, 3);
        done();
      });

      it('logs an object as a string', (done) => {
        const error = { statusCode: 500, message: 'internal server error' };
        log.error(error);
        sinon.assert.calledOnce(consoleErrorStub);
        done();
      });

      it('does not log when level is set to NONE', (done) => {
        log.setLevel(LogLevel.NONE);
        log.error('testtest');
        sinon.assert.callCount(consoleErrorStub, 0);
        done();
      });

      it('makes external call when level is set to ERROR', (done) => {
        log.setLevel(LogLevel.ERROR);
        log.setExternalLevel(LogLevel.ERROR);

        const externalStub = sandbox.stub(log, 'callToExternal');
        log.error('test');
        sinon.assert.calledOnce(externalStub);

        done();
      });

      it('does not make external call when externalLevel is set to NONE', (done) => {
        log.setLevel(LogLevel.ERROR);
        log.setExternalLevel(LogLevel.NONE);

        const externalStub = sandbox.stub(log, 'callToExternal');
        log.error('test');
        sinon.assert.callCount(externalStub, 0);

        done();
      });
    });

    describe('info', () => {
      let consoleInfoStub;
      let log;
      let sandbox;
      beforeEach(() => {
        sandbox = sinon.createSandbox();
        log = new Logman();
        log.setLevel(LogLevel.INFO);
        log.setExternalLevel(LogLevel.NONE);
        consoleInfoStub = sandbox.stub(console, 'info');
      });
      afterEach(() => {
        sandbox.restore();
      });

      it('logs a single line with a single log.info call', (done) => {
        log.info('test');
        sinon.assert.calledOnce(consoleInfoStub);
        done();
      });

      it('logs 3 line with a 3 argument log.info call', (done) => {
        log.info('test1', 'test2', 'test3');
        sinon.assert.callCount(consoleInfoStub, 3);
        done();
      });

      it('logs an object as a string', (done) => {
        const error = { statusCode: 500, message: 'internal server error' };
        log.info(error);
        sinon.assert.calledOnce(consoleInfoStub);
        done();
      });

      it('does not log when level is set to ERROR', (done) => {
        log.setLevel(LogLevel.ERROR);
        log.info('testtest');
        sinon.assert.callCount(consoleInfoStub, 0);
        done();
      });

      it('makes external call when level is set to INFO', (done) => {
        log.setLevel(LogLevel.INFO);
        log.setExternalLevel(LogLevel.INFO);

        const externalStub = sandbox.stub(log, 'callToExternal');
        log.info('test');
        sinon.assert.calledOnce(externalStub);

        done();
      });

      it('does not make external call when externalLevel is set to NONE', (done) => {
        log.setLevel(LogLevel.INFO);
        log.setExternalLevel(LogLevel.NONE);

        const externalStub = sandbox.stub(log, 'callToExternal');
        log.info('test');
        sinon.assert.callCount(externalStub, 0);

        done();
      });
    });
  });
});
